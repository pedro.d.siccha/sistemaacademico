<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Anioescolar extends Model
{
    protected $table = 'anioescolar';
    protected $fillable = ['id', 'nombre', 'descripcion', 'fecinicio', 'fecfin', 'tipoanioescolar_id'];

    public static function aneTae($id){
    	return Anioescolar::where('tipoanioescolar_id', '=', $id) -> get();
    }
}
