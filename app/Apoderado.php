<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Apoderado extends Model
{
    protected $table = 'apoderado';
    protected $fillable = ['id', 'nombre', 'apellido', 'fecnacimiento', 'edad', 'dni', 'direccion', 'telefono', 'correo'];
}
