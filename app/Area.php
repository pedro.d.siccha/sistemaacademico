<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'cursos';
    protected $fillable = ['id', 'nombre', 'horaasignada'];

}
