<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $table = 'asistencia';
    protected $fillable = ['id', 'fecha', 'hora', 'estado'];

}
