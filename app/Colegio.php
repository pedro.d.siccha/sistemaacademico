<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Colegio extends Model
{
    protected $table = 'colegio';
    protected $fillable = ['id', 'codModular', 'nombre'];
}
