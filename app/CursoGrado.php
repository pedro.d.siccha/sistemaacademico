<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class CursoGrado extends Model
{
    protected $table = 'curso_grado';
    protected $fillable = ['id', 'detalle', 'cursos_id', 'nivel_grado_id', 'empleado_id'];

    public static function cgrCur($id){
    	return CursoGrado::where('cursos_id', '=', $id) -> get();
    }

    public static function cgrGra($id){
        return CursoGrado::where('nivel_grado_id', '=', $id) -> get();
    }

    public static function cgrEmp($id){
        return CursoGrado::where('empleado_id', '=', $id) -> get();
    }
}
