<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = 'empleado';
    protected $fillable = ['id', 'nombre', 'apellido', 'dni', 'telefono', 'direccion', 'genero', 'titulo', 'foto', 'users_id', 'tipo_empleado_id'];

    public static function useEmp($id){
    	return Empleado::where('users_id', '=', $id) -> get();
    }
}
