<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table = 'estudiante';
    protected $fillable = ['id', 'nombre', 'apellido', 'dni', 'fecnacimiento', 'edad', 'direccion', 'telefono', 'correo', 'estado'];

}
