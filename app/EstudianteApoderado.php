<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class EstudianteApoderado extends Model
{
    protected $table = 'estudiante_apoderado';
    protected $fillable = ['id', 'detalle', 'apoderado_id', 'estudiante_id', 'parentezco_id'];

    public static function esaApo($id){
    	return EstudianteApoderado::where('apoderado_id', '=', $id) -> get();
    }

    public static function esaEst($id){
    	return EstudianteApoderado::where('estudiante_id', '=', $id) -> get();
    }

    public static function esaPar($id){
    	return EstudianteApoderado::where('parentezco_id', '=', $id) -> get();
    }
}
