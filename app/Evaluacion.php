<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Evaluacion extends Model
{
    protected $table = 'evaluacion';
    protected $fillable = ['id', 'nota'];

    public static function aneTae($id){
    	return Anioescolar::where('tipoanioescolar_id', '=', $id) -> get();
    }
}
