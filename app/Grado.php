<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $table = 'grado';
    protected $fillable = ['id', 'nombre'];

}
