<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class GradoSeccion extends Model
{
    protected $table = 'grado_seccion';
    protected $fillable = ['id', 'grado_id', 'seccion_id'];
    protected $guarded = ['id'];

    public function seccion()
    {
        return $this->belongsTo(Seccion::class, 'grado_seccion');
    }

    public function grado()
    {
        return $this->belongsTo(Grado::class, 'grado_seccion');
    }
}
