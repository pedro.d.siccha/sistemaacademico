<?php

namespace GestionAcademica\Http\Controllers;

use Illuminate\Http\Request;
use GestionAcademica\Anioescolar;
use GestionAcademica\Grado;
use GestionAcademica\Seccion;
use GestionAcademica\Area;
use GestionAcademica\CursoGrado;
use GestionAcademica\GradoSeccion;
use GestionAcademica\Nivel;
use GestionAcademica\NivelGrado;
use Illuminate\Support\Facades\Auth;

class AcademicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function anioEscolar()
    {
        $anioEscolar = \DB::SELECT('SELECT * FROM anioescolar');

        $tipoAnio = \DB::SELECT('SELECT * FROM tipoanioescolar');

        return view('academica.anioescolar', compact('anioEscolar', 'tipoAnio'));
    }

    public function crearAnioEscolar(Request $request)
    {
        $anio = new Anioescolar();
        $anio->nombre = $request->nombre;
        $anio->descripcion = $request->descripcion;
        $anio->fecinicio = $request->fecinicio;
        $anio->fecfin = $request->fecfin;
        $anio->tipoanioescolar_id = $request->tipoAnio;

        if ($anio->save()) {

            $anioEscolar = \DB::SELECT('SELECT * FROM anioescolar');

            return response()->json(["view"=>view('academica.listAnioEscolar',compact('anioEscolar'))->render()]);
        }
    }

    public function area()
    {
        $curso = \DB::SELECT('SELECT * FROM cursos');

        return view('academica.area', compact('curso'));
    }

    public function crearArea(Request $request){
        $res = 0;
        $curso = new Area();
        $curso->nombre = $request->nombre;
        $curso->horaasignada = "";

        if ($curso->save()) {

            $res = 1;
            
        }

        $curso = \DB::SELECT('SELECT * FROM cursos');

        return response()->json(["view"=>view('academica.listArea',compact('curso'))->render(), "res" => $res]);
    }

    public function planEstudios()
    {
        $nivel = \DB::SELECT('SELECT * FROM nivel');

        $curso = \DB::SELECT('SELECT * FROM cursos');

        $gradoCurso = \DB::SELECT('SELECT cg.id AS cursoGrado_id, c.id AS cursos_id, c.nombre AS cursos, ng.id AS nivelGrado_id, n.id AS nivel_id, n.nombre AS nivel, n.anioescolar_id AS anioEscolar_id, g.id AS grado_id, g.nombre AS grado
                                   FROM curso_grado cg, cursos c, nivel_grado ng, nivel n, grado g
                                   WHERE cg.cursos_id = c.id AND cg.nivel_grado_id = ng.id AND ng.nivel_id = n.id AND ng.grado_id = g.id');

        $docenteCurso = \DB::SELECT('SELECT e.nombre, e.apellido, e.foto, g.nombre AS grado, n.nombre AS nivel, c.nombre AS cursos
                                     FROM curso_grado cg, empleado e, nivel_grado ng, grado g, nivel n, cursos c
                                     WHERE cg.empleado_id = e.id AND cg.nivel_grado_id = ng.id AND ng.grado_id = g.id AND ng.nivel_id = n.id AND cg.cursos_id = c.id');

        return view('academica.planestudios', compact('curso', 'nivel', 'gradoCurso', 'docenteCurso'));
    }

    public function nivel()
    {

        $anioescolar = \DB::SELECT('SELECT * FROM anioescolar');

        $nivel = \DB::SELECT('SELECT * FROM nivel');

        return view('academica.nivel', compact('nivel', 'anioescolar'));
    }

    public function guardarNivel(Request $request)
    {
        $anioescolar_id = $request->anioescolar_id;
        $nombre = $request->nombre;
        $detalle = $request->detalle;
        $res = 0;

        $niv = new Nivel();
        $niv->nombre = $nombre;
        $niv->detalle = $detalle;
        $niv->anioescolar_id = $anioescolar_id;
        if ($niv->save()) {
            $res = 1;
        }

        $nivel = \DB::SELECT('SELECT * FROM nivel');

        return response()->json(["view"=>view('academica.listNivel', compact('nivel'))->render(),"res" => $res]);

    }

    public function grado()
    {
        $grado = \DB::SELECT('SELECT * FROM grado');

        $seccion = \DB::SELECT('SELECT * FROM seccion');

        return view('academica.grado', compact('grado', 'seccion'));
    }

    public function crearGrado(Request $request)
    {

        $res = 0;

        $grado = new Grado();
        $grado->nombre = $request->nombre;

        if ($grado->save()) {
            $res = 1;
        }
        $grado = \DB::SELECT('SELECT * FROM grado');
        return response()->json(["view"=>view('academica.listGrado',compact('grado'))->render(), "res" => $res]);

    }

    public function seccion()
    {
        $seccion = \DB::SELECT('SELECT * FROM seccion');

        return view('academica.seccion', compact('seccion'));
    }

    public function crearSeccion(Request $request)
    {
        $res = 0;

        $seccion = new Seccion();
        $seccion->nombre = $request->nombre;

        if ($seccion->save()) {
            $res = 1;
        }
        $seccion =  \DB::SELECT('SELECT * FROM seccion');

        return response()->json(["view"=>view('academica.listSeccion',compact('seccion'))->render(), "res" => $res]);
    }

    public function asignacion()
    {
        $listNivel = \DB::SELECT('SELECT * FROM nivel');

        $NivelGrado = NivelGrado::with('grado')->get()->pluck('id', 'grado_id')->toArray();

        $listGrado = \DB::SELECT('SELECT * FROM grado');
        $listSeccion = \DB::SELECT('SELECT * FROM seccion');
        $listCursos = \DB::SELECT('SELECT * FROM cursos');

        return view('academica.asignacion', compact('listNivel', 'listGrado', 'listSeccion', 'listCursos', 'NivelGrado'));
    }

    public function horarios()
    {

        $nivel = \DB::SELECT('SELECT * FROM nivel');

        $grado = \DB::SELECT('SELECT * FROM grado');

        return view('academica.horarios', compact('grado', 'nivel'));
    }

    public function mostrarHorario(Request $request)
    {
        $idNivel = $request->idNivel;

        $grado = \DB::SELECT('SELECT ng.id AS nivelGrado_id, g.id AS grado_id, g.nombre AS grado 
                              FROM nivel_grado ng, grado g 
                              WHERE ng.grado_id = g.id AND nivel_id = "'.$idNivel.'"');

        $cursos = \DB::SELECT('SELECT cg.id AS cursoGrado_id, c.id AS curso_id, c.nombre AS curso, n.nombre AS nivel
                               FROM curso_grado cg, cursos c, nivel_grado ng, nivel n
                               WHERE cg.cursos_id = c.id AND cg.nivel_grado_id = ng.id AND ng.nivel_id = n.id AND ng.nivel_id = "'.$idNivel.'"');

        return response()->json(["view"=>view('academica.tabHorarios', compact('grado', 'cursos'))->render()]);

    }

    public function generarHorario(Request $request)
    {
        return response()->json(["view"=>view('academica.listHorarios')->render()]);
    }

    public function verHorario(Request $request)
    {
        return response()->json(["view"=>view('academica.verHorario')->render()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function asignarCurso(Request $request)
    {
        $idCursos = $request->idCursos;

        
 
        for ($i=0; $i < count($idCursos) ; $i++) { 
            
            $idLlenar=$idCursos[$i];
            $curGrado = new CursoGrado();
            $curGrado->detalle = "";
            $curGrado->cursos_id = $idCursos[$i];
            $curGrado->grado_id = $request->grado_id;
            $curGrado->empleado_id = 1;
            $curGrado->save();
            
            
        }
    }

    public function asignarGrado(Request $request)
    {
        $idGrado = $request->idGrado;
        $res = 0;
        $txtResp = "";

        for ($i=0; $i < count($idGrado) ; $i++) { 
            
            $idLlenar=$idGrado[$i];
            $ng = new NivelGrado();
            $ng->grado_id = $idGrado[$i];
            $ng->nivel_id = $request->nivel_id;
            if ($ng->save()) {
                $res = 1;
                $txtResp = "";
            }else {
                $txtResp = "Error ACxNg00" + $i;
            }
            
        }

        $nivelGrado = \DB::SELECT('SELECT ng.id AS idNivelGrado, g.id AS idGrado, n.id AS idNivel, n.nombre AS nivel, g.nombre AS grado 
                                   FROM nivel_grado ng, nivel n, grado g
                                   WHERE ng.nivel_id = n.id AND ng.grado_id = g.id
                                   AND n.id = "'.$request->nivel_id.'"');


        return response()->json(["view"=>view('academica.tabNivelGrado', compact('nivelGrado'))->render(), "res" => $res, "txtResp" => $txtResp]);

    }

    public function asignarSeccion(Request $request)
    {
        $idSeccion = $request->idSeccion;
        $res = 0;
        $txtResp = "";

        for ($i=0; $i < count($idSeccion) ; $i++) { 
            
            $idLlenar=$idSeccion[$i];
            $ns = new GradoSeccion();
            $ns->grado_id = $request->grado_id;
            $ns->seccion_id = $idSeccion[$i];
            if ($ns->save()) {
                $res = 1;
                $txtResp = "";
            }else {
                $res = 0;
                $txtResp = "Error ACxGs00" + $i;
            }
            
        }

        $gradoSeccion = \DB::SELECT('SELECT gs.id AS gradoSeccion_id, gs.grado_id AS grado_id, gs.seccion_id AS seccion_id, g.nombre AS grado, s.nombre AS seccion 
                                     FROM grado_seccion gs, grado g, seccion s
                                     WHERE gs.grado_id = g.id AND gs.seccion_id = s.id AND gs.grado_id = "'.$request->grado_id.'"');

        return response()->json(["view"=>view('academica.tabGradoSeccion', compact('gradoSeccion'))->render(), "res" => $res, "txtResp" => $txtResp]);

    }

    public function asignarCursos(Request $request)
    {
        $idCursos = $request->idCursos;
        $res = 0;
        $txtResp = "";

        for ($i=0; $i < count($idCursos) ; $i++) { 
            
            $idLlenar=$idCursos[$i];
            $cg = new CursoGrado();
            $cg->nivel_grado_id = $request->grado_id;
            $cg->cursos_id = $idCursos[$i];
            if ($cg->save()) {
                $res = 1;
                $txtResp = "";
            }else {
                $res = 0;
                $txtResp = "Error ACxAc00" + $i;
            }
            
        }
/*
        $gradoSeccion = \DB::SELECT('SELECT gs.id AS gradoSeccion_id, gs.grado_id AS grado_id, gs.seccion_id AS seccion_id, g.nombre AS grado, s.nombre AS seccion 
                                     FROM grado_seccion gs, grado g, seccion s
                                     WHERE gs.grado_id = g.id AND gs.seccion_id = s.id AND gs.grado_id = "'.$request->grado_id.'"');
*/

        return response()->json(["res" => $res, "txtResp" => $txtResp]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verGrados(Request $request)
    {
        $idNivel = $request->idNivel;

        $nivelGrado = \DB::SELECT('SELECT ng.id AS idNivelGrado, g.id AS idGrado, n.id AS idNivel, n.nombre AS nivel, g.nombre AS grado 
                                   FROM nivel_grado ng, nivel n, grado g
                                   WHERE ng.nivel_id = n.id AND ng.grado_id = g.id
                                   AND n.id = "'.$idNivel.'"');

        return response()->json(["view"=>view('academica.tabNivelGrado', compact('nivelGrado'))->render()]);

    }

    public function verGradosDocente(Request $request)
    {
        $idNivel = $request->idNivel;

        $nivelGrado = \DB::SELECT('SELECT ng.id AS idNivelGrado, g.id AS idGrado, n.id AS idNivel, n.nombre AS nivel, g.nombre AS grado 
                                   FROM nivel_grado ng, nivel n, grado g
                                   WHERE ng.nivel_id = n.id AND ng.grado_id = g.id
                                   AND n.id = "'.$idNivel.'"');

        return response()->json(["view"=>view('academica.listNivelGrado', compact('nivelGrado'))->render()]);

    }

    public function verCursosDocente(Request $request)
    {
        $idNivelGrado = $request->idNivelGrado;

        $cursoGrado = \DB::SELECT('SELECT cg.id AS cursoGrado_id, c.id AS cursos_id, c.nombre AS cursos, ng.id AS nivelGrado_id, n.id AS nivel_id, n.nombre AS nivel, n.anioescolar_id AS anioEscolar_id, g.id AS grado_id, g.nombre AS grado, cg.empleado_id AS docente_id, e.nombre AS nomDocente, e.apellido AS apeDocente
                                   FROM curso_grado cg
                                   INNER JOIN cursos c ON cg.cursos_id = c.id
                                   LEFT JOIN empleado e ON cg.empleado_id = e.id
                                   INNER JOIN nivel_grado ng ON cg.nivel_grado_id = ng.id
                                   INNER JOIN nivel n ON ng.nivel_id = n.id
                                   INNER JOIN grado g ON ng.grado_id = g.id 
                                   WHERE ng.id = "'.$idNivelGrado.'"');

        return response()->json(["view"=>view('academica.tablaCursos', compact('cursoGrado'))->render()]);


    }

    public function verSeccion(Request $request)
    {
        $idGrado = $request->idGrado;

        $gradoSeccion = \DB::SELECT('SELECT gs.id AS gradoSeccion_id, gs.grado_id AS grado_id, gs.seccion_id AS seccion_id, g.nombre AS grado, s.nombre AS seccion 
                                     FROM grado_seccion gs, grado g, seccion s
                                     WHERE gs.grado_id = g.id AND gs.seccion_id = s.id AND gs.grado_id = "'.$idGrado.'"');

        return response()->json(["view"=>view('academica.tabGradoSeccion', compact('gradoSeccion'))->render()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrarGradoPlanEstudio(Request $request)
    {
        $idNivel = $request->idNivel;

        $grado = \DB::SELECT('SELECT ng.id AS nivelGrado_id, ng.nivel_id AS nivel_id, ng.grado_id AS grado_id, g.nombre AS grado, n.nombre AS nivel
                              FROM nivel_grado ng, nivel n, grado g
                              WHERE ng.nivel_id = n.id AND ng.grado_id = g.id AND n.id = "'.$idNivel.'"');

        return response()->json(["view"=>view('academica.tabGradoPlanEstudio', compact('grado'))->render(), "idNivel" => $idNivel]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verDocenteDNI(Request $request)
    {
        $dni = $request->dniDocente;
        $res = 0;

        $docente = \DB::SELECT('SELECT e.id AS docente_id, e.nombre AS nombre, e.apellido AS apellido, e.dni AS dni, e.telefono AS telefono, e.direccion AS direccion, e.genero AS genero, e.titulo AS titulo, e.foto AS foto
                                FROM empleado e, tipo_empleado te
                                WHERE e.tipo_empleado_id = te.id AND e.dni = "'.$dni.'" AND te.nombre LIKE "%DOCENTE%"');

        if ($docente != null) {
            $res = 1;
        }else {
            $res = 0;
        }

        return response()->json(["view"=>view('academica.tabPerfilDocente', compact('docente'))->render(), "res" => $res]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function asignarDocente(Request $request)
    {
        $idDocente = $request->idDocente;
        $idCursoGrado = $request->cursoGrado_id;
        $res = 0;
        $resText = "";
        
        $cg = CursoGrado::where('id', '=', $idCursoGrado)->first();
        $cg->empleado_id = $idDocente;
        if ($cg->save()) {
            $res = 1;
            $resText = "";
        }else{
            $res = 0;
            $resText = "ACxAd01";
        }

        $docenteCurso = \DB::SELECT('SELECT e.nombre, e.apellido, e.foto, g.nombre AS grado, n.nombre AS nivel, c.nombre AS cursos
                                     FROM curso_grado cg, empleado e, nivel_grado ng, grado g, nivel n, cursos c
                                     WHERE cg.empleado_id = e.id AND cg.nivel_grado_id = ng.id AND ng.grado_id = g.id AND ng.nivel_id = n.id AND cg.cursos_id = c.id');

        return response()->json(["view"=>view('academica.tabDocenteCurso', compact('docenteCurso'))->render(), "res" => $res, "resText"=>$resText]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
