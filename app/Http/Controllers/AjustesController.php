<?php

namespace GestionAcademica\Http\Controllers;

use GestionAcademica\Parentezco;
use GestionAcademica\TipoAnioEscolar;
use GestionAcademica\TipoEmpleado;
use GestionAcademica\TipoPeriodo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AjustesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajustesBasicos()
    {
        $tipoanioescolar = \DB::SELECT('SELECT * FROM tipoanioescolar');

        $tipoempleado = \DB::SELECT('SELECT * FROM tipo_empleado');

        $tipoperiodo = \DB::SELECT('SELECT * FROM tipoperiodo');

        $parentezco = \DB::SELECT('SELECT * FROM parentezco');

        return view('ajustesgenerales.basico', compact('tipoanioescolar', 'tipoempleado', 'tipoperiodo', 'parentezco'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarTAnio(Request $request)
    {
        $res = 0;
        $txtResp = "";
        $tipoAnioEscolar = $request->tipoAnioEscolar;

        $tae = new TipoAnioEscolar();
        $tae->tipoanio = $tipoAnioEscolar;
        if ($tae->save()) {
            $res = 1;
            $txtResp = "";
        }else{
            $res = 0;
            $txtResp = "ACxGtA01";
        }

        $tipoanioescolar = \DB::SELECT('SELECT * FROM tipoanioescolar');

        return response()->json(["view"=>view('ajustesgenerales.tabTAnioEscolar', compact('tipoanioescolar'))->render(), "res" => $res, "txtResp" => $txtResp]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardarTPeriodo(Request $request)
    {
        $res = 0;
        $txtResp = "";
        $tipoPeriodo = $request->tipoPeriodo;
        $duracion = $request->duracion;

        $tp = new TipoPeriodo();
        $tp->nombre = $tipoPeriodo;
        $tp->duracion = $duracion;
        if ($tp->save()) {
            $res = 1;
            $txtResp = "";
        }else {
            $res = 0;
            $txtResp = "ACxGtP01";
        }

        $tipoperiodo = \DB::SELECT('SELECT * FROM tipoperiodo');

        return response()->json(["view"=>view('ajustesgenerales.tabTPeriodo', compact('tipoperiodo'))->render(), "res" => $res, "txtResp" => $txtResp]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function guardarTEmpleado(Request $request)
    {
        $res = 0;
        $txtResp = "";
        $tipoEmpleado = $request->tipoEmpleado;
        $detalles = $request->detalles;

        $te = new TipoEmpleado();
        $te->nombre = $tipoEmpleado;
        $te->detalles = $detalles;

        if ($te->save()) {
            $res = 1;
            $txtResp = "";
        }else {
            $res = 0;
            $txtResp = "ACxGtE01";
        }

        $tipoempleado = \DB::SELECT('SELECT * FROM tipo_empleado');

        return response()->json(["view"=>view('ajustesgenerales.tabTEmpleado', compact('tipoempleado'))->render(), "res" => $res, "txtResp" => $txtResp]);

    }

    public function guardarParentezco(Request $request)
    {
        $res = 0;
        $txtResp = "";
        $parentezco = $request->parentezco;

        $p = new Parentezco();
        $p->parentezco = $parentezco;

        if ($p->save()) {
            $res = 1;
            $txtResp = "";
        }else {
            $res = 0;
            $txtResp = "ACxGP01";
        }

        $parentezco = \DB::SELECT('SELECT * FROM parentezco');

        return response()->json(["view"=>view('ajustesgenerales.tabParentezco', compact('parentezco'))->render(), "res" => $res, "txtResp" => $txtResp]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
