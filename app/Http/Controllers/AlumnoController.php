<?php

namespace GestionAcademica\Http\Controllers;

use Illuminate\Http\Request;
use GestionAcademica\Estudiante;
use GestionAcademica\Apoderado;
use GestionAcademica\EstudianteApoderado;
use GestionAcademica\Matricula;
use Illuminate\Support\Facades\Auth;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crearEstudiante()
    {
        $parentezco = \DB::SELECT('SELECT * FROM parentezco');
 
        return view('alumno.crear', compact('parentezco'));
    }

    public function listaAlumnos()
    {

        $nivel = \DB::SELECT('SELECT * FROM nivel');

        $alumno = \DB::SELECT('SELECT m.id AS matricula_id, e.id AS estudiante_id, e.nombre AS nombre, e.apellido AS apellido, e.dni AS dni, g.id AS grado_id, g.nombre AS grado 
                               FROM matricula m, estudiante e, grado g
                               WHERE m.estudiante_id = e.id AND m.grado_id = g.id AND m.estado = "ACTIVO"');

        return view('alumno.listaAlumno', compact('alumno', 'nivel'));
    }

    public function verCbGrado(Request $request)
    {
        $idNivel = $request->idNivel;

        $grado = \DB::SELECT('SELECT ng.id AS nivelGrado_id, n.id AS nivel_id, n.nombre AS nivel, g.id AS grado_id, g.nombre AS grado
                              FROM nivel_grado ng, nivel n, grado g
                              WHERE ng.nivel_id = n.id AND ng.grado_id = g.id AND n.id = "'.$idNivel.'"');

        return response()->json(["view"=>view('alumno.cbGrado', compact('grado'))->render()]);

    }

    public function verCbSeccion(Request $request)
    {
        $idGrado = $request->idGrado;

        $seccion = \DB::SELECT('SELECT gs.id AS gradoSeccion_id, g.id AS grado_id, g.nombre AS grado, s.id AS seccion_id, s.nombre AS seccion
                                FROM grado_seccion gs, grado g, seccion s
                                WHERE gs.grado_id = g.id AND gs.seccion_id = s.id AND g.id = "'.$idGrado.'"');

        return response()->json(["view"=>view('alumno.cbSeccion', compact('seccion'))->render()]);

    }

    public function cambioSeccion()
    {
        return view('alumno.cambioSeccion');
    }

    public function retiroEstudiante()
    {
        $alumno = \DB::SELECT('SELECT * FROM estudiante');
 
        return view('alumno.retiro', compact('alumno'));
    }

    public function retirarAlumno(Request $request)
    {
        /*
        $estudiante_id = $request->estudiante_id;
        $matricula_id = $request->matricula_id;

        $matricula = Matricula::find($matricula_id);
        if ($matricula->delete()) {
            $estudiante = Estudiante::find($estudiante_id);
            if ($estudiante->delete()) {
                $alumno = \DB::SELECT('SELECT e.id AS estudiante_id, CONCAT(e.nombre, " ", e.apellido) AS alumno, e.dni, g.nombre AS grado, s.nombre AS seccion, m.id AS matricula_id
                               FROM estudiante e, matricula m, grado g, seccion s
                               WHERE m.estudiante_id = e.id AND m.grado_id = g.id AND g.seccion_id = s.id');

                return response()->json(["view"=>view('alumno.listAlumRetiro',compact('alumno'))->render()]);
            }
        }
        */
        return response()->json(["notificacion"=>view('notificacion.retirado')->render()]);

    }

    public function banAlumno(Request $request)
    {
        $res = 0;
        $resText = "";
        $idEstudiante = $request->idEstudiante;

        $matricula = \DB::SELECT('SELECT id FROM matricula WHERE estudiante_id = "'.$idEstudiante.'" AND estado = "ACTIVO"');

        $mat = Matricula::where("id", "=", $matricula[0]->id)->first();
        $mat->estado = "RETIRADO";
        if ($mat->save()) {
            $est = Estudiante::where("id", "=", $idEstudiante)->first();
            $est->estado = "RETIRADO";
            if ($est->save()) {
                $res = 1;
                $resText = "";
            }else {
                $res = 0;
                $resText = "ACxBa02";
            }
        }else {
            $res = 0;
            $resText = "ACxBa01";
        }

        $alumno = \DB::SELECT('SELECT m.id AS matricula_id, e.id AS estudiante_id, e.nombre AS nombre, e.apellido AS apellido, e.dni AS dni, g.id AS grado_id, g.nombre AS grado 
                               FROM matricula m, estudiante e, grado g
                               WHERE m.estudiante_id = e.id AND m.grado_id = g.id AND m.estado = "ACTIVO"');

        return response()->json(["web"=>view('alumno.tabAlumnoMatriculado', compact('alumno'))->render(), "res"=>$res, "resText"=>$resText]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarEstudiante(Request $request)
    {
        $resp = 0;

        $est = new Estudiante();
        $est->nombre = $request->aluNombre;
        $est->apellido = $request->aluApellido;
        $est->fecNacimiento = $request->aluFecNacimiento;
        $est->edad = $request->aluEdad;
        $est->dni = $request->aluDni;
        $est->direccion = $request->aluDireccion;
        $est->telefono = $request->aluTelefono;
        $est->correo = $request->aluCorreo;
        $est->estado = "NUEVO";

        if ($est->save()) {
            
            $apo = new Apoderado();
            $apo->nombre = $request->acoNombre;
            $apo->apellido = $request->acoApellido;
            $apo->fecNacimiento = $request->acoFecNacimiento;
            $apo->edad = $request->acoEdad;
            $apo->dni = $request->acoDni;
            $apo->direccion = $request->acoDireccion;
            $apo->telefono = $request->acoTelefono;
            $apo->correo = $request->acoCorreo;

            if ($apo->save()) {
                $estudiante = \DB::SELECT('SELECT MAX(id) AS id FROM estudiante');
                $apoderado = \DB::SELECT('SELECT MAX(id) AS id FROM apoderado');

                $estApo = new EstudianteApoderado();
                $estApo->detalle = "";
                $estApo->apoderado_id = $apoderado[0]->id;
                $estApo->estudiante_id = $estudiante[0]->id;
                $estApo->parentezco_id = $request->parentezco_id;

                if ($estApo->save()) {
                    $resp = 1;
                    return response()->json(["notificacion"=>view('notificacion.correcto')->render(), 'resp'=>$resp]);
                }   
            }
        }
        return response()->json(["notificacion"=>view('notificacion.error')->render(), 'resp'=>$resp]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function mostrarAlumnos(Request $request)
    {

        $alumnos = \DB::SELECT('SELECT * FROM estudiante');

        return response()->json(["view"=>view('alumno.tabAlumnos', compact('alumnos'))->render()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verificarDNI(Request $request)
    {
        $dni = $request->dni;

        $ver = \DB::SELECT('SELECT * FROM estudiante WHERE dni = "'.$dni.'";');

        if ($ver == null) {
            $resp = 0;
        }else {
            $resp = 1;
        }

        return response()->json(['resp'=>$resp]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
