<?php

namespace GestionAcademica\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AsistenciaController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function asistencia()
    {
        $curso = \DB::SELECT('SELECT cg.id AS cursoGrado_id, c.id AS curso_id, c.nombre AS curso, ng.id AS nivelGrado_ng, g.id AS grado_id, g.nombre AS grado 
                              FROM curso_grado cg, cursos c, nivel_grado ng, grado g
                              WHERE cg.cursos_id = c.id AND cg.nivel_grado_id = ng.id AND ng.grado_id = g.id AND ng.nivel_id = "14"');

        return view('asistencia.asistencia', compact('curso'));
    }

    public function buscarAlumnoAsistencia(Request $request)
    {
        
        $estudiantes = \DB::SELECT('SELECT m.id AS matricula_id, e.id AS estudiante_id, e.nombre AS nombre, e.apellido AS apellido, e.dni AS dni 
                                    FROM matricula m, estudiante e
                                    WHERE m.estudiante_id = e.id AND m.estado = "ACTIVO" AND m.grado_id = "'.$request->idGrado.'"');

        return response()->json(["tabAlumnos"=>view('asistencia.divListaAlumno', compact('estudiantes'))->render()]);
    }

    public function enviarAsistencia()
    {

        $estudiantes = \DB::SELECT('SELECT m.id AS matricula_id, e.id AS estudiante_id, e.nombre AS nombre, e.apellido AS apellido, e.dni AS dni 
                                    FROM matricula m, estudiante e
                                    WHERE m.estudiante_id = e.id AND m.estado = "ACTIVO" AND m.grado_id = "46"');

        return response()->json(["notificacion"=>view('asistencia.tabCantAsistencia')->render(), "asistencia"=>view('asistencia.tabAsistencia', compact('estudiantes'))->render()]);
    }

    public function asistenciaDocente()
    {

        $nivel = \DB::SELECT('SELECT * FROM nivel');

        return view('asistencia.asistenciadocente', compact('nivel'));
    }

    public function buscarDocenteAsistencia()
    {
        $docente = \DB::SELECT('SELECT te.id AS tipoEmpleado_id, e.id AS empleado_id, e.dni AS dni, e.nombre AS nombre, e.apellido AS apellido 
                                FROM empleado e, tipo_empleado te
                                WHERE e.tipo_empleado_id = te.id AND te.nombre = "DOCENTE"');

        return response()->json(["tabDocente"=>view('asistencia.divListaDocente', compact('docente'))->render()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verDocenteAsistencia(Request $request)
    {
        $idNivel = $request->idNivel;

        $docentes = \DB::SELECT('SELECT cg.id AS cursoGrado_id, c.id AS curso_id, c.nombre AS curso, e.id AS empleado_id, e.nombre AS nombre, e.apellido AS apellido, e.dni AS dni, e.foto AS foto, ng.id AS nivelGrado_id, n.id AS nivel_id, n.nombre AS nivel, g.id AS grado_id, g.nombre AS grado, tp.id AS tipoEmpleado_id
                                 FROM curso_grado cg, cursos c, empleado e, nivel_grado ng, nivel n, grado g, tipo_empleado tp
                                 WHERE cg.cursos_id = c.id AND cg.empleado_id = e.id AND cg.nivel_grado_id = ng.id AND ng.nivel_id = n.id AND ng.grado_id = g.id AND e.tipo_empleado_id = tp.id
                                 AND tp.nombre = "DOCENTE" AND n.id = "'.$idNivel.'"');

        return response()->json(["tabDocente"=>view('asistencia.tabDocente')->render(), compact('docentes')]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
