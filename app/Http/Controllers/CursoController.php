<?php

namespace GestionAcademica\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function administrarCursos()
    {
        $cursos = \DB::SELECT('SELECT * FROM cursos');
 
        return view('cursos.curso', compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function administrarExamen()
    {
        $cursos = \DB::SELECT('SELECT * FROM cursos');

        return view('cursos.examen', compact('cursos'));
    }

    public function administraNotas()
    {
        $cursos = \DB::SELECT('SELECT * FROM cursos');

        return view('cursos.notas', compact('cursos'));
    }

    public function administrarAsistencia()
    {
        $cursos = \DB::SELECT('SELECT * FROM cursos');

        return view('cursos.asistencia', compact('cursos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function panelCursos(Request $request)
    {
        return response()->json(["panelCursos"=>view('cursos.divPanelCursos')->render()]);
    }

    public function panelExamen(Request $request)
    {
        return response()->json(["panelExamen"=>view('cursos.divPanelExamen')->render()]);
    }

    public function panelNotas(Request $request)
    {
        return response()->json(["panelNotas"=>view('cursos.divPanelNotas')->render()]);
    }

    public function panelAsistencia(Request $request)
    {
        return response()->json(["panelAsistencia"=>view('cursos.divPanelAsistencia')->render()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
