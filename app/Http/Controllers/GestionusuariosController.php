<?php

namespace GestionAcademica\Http\Controllers;

use GestionAcademica\Empleado;
use GestionAcademica\User;
use Illuminate\Http\Request;
use Storage;
use Illuminate\Support\Facades\Auth;

class GestionusuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function padres()
    {
        return view('gestionusuarios.padres');
    }

    public function docentes()
    {
        $docente = \DB::SELECT('SELECT e.id AS docente_id, e.nombre, e.apellido, e.dni, e.telefono, e.direccion, e.genero, e.titulo, e.foto, u.name AS usuario
                                FROM empleado e, users u
                                WHERE e.users_id = u.id');

        return view('gestionusuarios.docentes', compact('docente'));
    }

    public function alumnos()
    {
        return view('gestionusuarios.alumnos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarDocente(Request $request)
    {
        $subido="";
        $urlGuardar="";
        $res = 0;
        $resText = "";
        $nombre = $request->nombre;
        $apellido = $request->apellidos;
        $dni = $request->dni;
        $correo = $request->correo;
        $telefono = $request->telefono;
        $direccion = $request->direccion;
        $genero = $request->a;
        $titulo = $request->titulo;
        $img = $request->foto;

        if ($request->hasFile('foto')) { 

            $extension = $img->getClientOriginalExtension();
            $nuevoNombre=$dni.".".$extension;
            $subido = Storage::disk('docente')->put($nuevoNombre, \File::get($img));
    
            if($subido){
                $urlGuardar='img/docente/'.$nuevoNombre;
            }
    
        }

        $user = new User();
        $user->name = $dni;
        $user->email = $correo;
        $user->password = bcrypt($dni);
        if ($user->save()) {
            $user = \DB::SELECT('SELECT id FROM users WHERE name = "'.$dni.'"');
            $tipoEmpleado = \DB::SELECT('SELECT id FROM tipo_empleado WHERE nombre = "DOCENTE"');

            $doc = new Empleado();
            $doc->nombre = $nombre;
            $doc->apellido = $apellido;
            $doc->dni = $dni;
            $doc->telefono = $telefono;
            $doc->direccion = $direccion;
            $doc->genero = $genero;
            $doc->titulo = $titulo;
            $doc->foto = $urlGuardar;
            $doc->users_id = $user[0]->id;
            $doc->tipo_empleado_id = $tipoEmpleado[0]->id;
            if ($doc->save()) {
                $res = 1;
                $resText = "";
            }else {
                $res = 0;
                $resText = "GUxGd01";
            }
        }else{
            $res = 0;
            $resText = "GUxGd02";
        }

        $docente = \DB::SELECT('SELECT e.id AS docente_id, e.nombre, e.apellido, e.dni, e.telefono, e.direccion, e.genero, e.titulo, e.foto, u.name AS usuario
                                FROM empleado e, users u
                                WHERE e.users_id = u.id');

        return response()->json(["view"=>view('ajustesgenerales.tabDocente',compact('docente'))->render(), 'res'=>$res, 'resText'=>$resText]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
