<?php

namespace GestionAcademica\Http\Controllers;

use Illuminate\Http\Request;
use GestionAcademica\Matricula;
use GestionAcademica\Colegio;
use GestionAcademica\Estudiante;
use GestionAcademica\Traslado;
use Storage;
use Illuminate\Support\Facades\Auth;

class MatriculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function matricula()
    {

        $nivel = \DB::SELECT('SELECT * FROM nivel');

        return view('matricula.matricula', compact('nivel'));
    }

    public function matriculaIndividual(Request $request)
    {

        $res = 0;
        $resText = "";
        $estudiante_id = $request->estudiante_id;
        $grado_id = $request->grado_id;

        $anioescolar_id = \DB::SELECT('SELECT MAX(id) AS id FROM anioescolar');

        $mat = new Matricula();
        $mat->estado = "ACTIVO";
        $mat->estudiante_id = $estudiante_id;
        $mat->grado_id = $grado_id;
        $mat->anioescolar_id = $anioescolar_id[0]->id;
        if ($mat->save()) {
            $es = Estudiante::where('id', '=', $estudiante_id)->first();
            $es->estado = "MATRICULADO";
            if ($es->save()) {
                $res = 1;
                $resText = "";
            }else{
                $res = 0;
                $resText = "MCxMi02";
            }
        }else {
            $res = 0;
            $resText = "MCxMi01";
        }

        return response()->json(["view"=>view('notificacion.correcto')->render(), "res"=>$res, "resText"=>$resText]);
    }

    public function generarMatriculaMasiva(Request $request)
    {
        $idEstudiante = $request->idEstudiante;
        $idGrado = $request->grado_id;
        $idAnioEscolar = \DB::SELECT('SELECT MAX(id) AS id FROM anioescolar');

        for ($i=0; $i < COUNT($idEstudiante) ; $i++) { 
            
            $mat = new Matricula();
            $mat->estado = "ACTIVO";
            $mat->estudiante_id = $idEstudiante[$i];
            $mat->grado_id = $idGrado;
            $mat->anioescolar_id = $idAnioEscolar[0]->id;
            if ($mat->save()) {
                $es = Estudiante::where('id', '=', $idEstudiante[$i])->first();
                $es->estado = "MATRICULADO";
                if ($es->save()) {
                    $res = 1;
                    $resText = "";
                }else {
                    $res = 0;
                    $resText = "MCxGmM02";
                }
            }else {
                $res = 0;
                $resText = "MCxGmM01";
            }
            
        }

        return response()->json(["res"=>$res, "resText"=>$resText]);

    }

    public function buscarAlumnoMatricula(Request $request)
    {
        $alumno = $request->alumno; 

        $estudiante = \DB::SELECT('SELECT * FROM estudiante WHERE nombre LIKE "%'.$alumno.'%" OR dni LIKE "%'.$alumno.'%"');
        
        if ($estudiante != null) {

            $matricula = \DB::SELECT('SELECT * FROM matricula m, estudiante e WHERE m.estudiante_id = "'.$estudiante[0]->id.'" AND m.estudiante_id = e.id');

            if ($matricula != null) {

                
            $gradoSeccion = \DB::SELECT('SELECT m.id AS matricula_id, g.id AS grado_id, g.nombre AS grado, s.id AS seccion_id, s.nombre AS seccion FROM matricula m, grado g, seccion s WHERE m.grado_id = g.id AND g.seccion_id = s.id AND m.estudiante_id = "'.$matricula[0]->id.'"');
            
            return response()->json(["view"=>view('matricula.datosAlumno',compact('matricula', 'gradoSeccion'))->render()]);

            }else {

                $gradoSeccion = \DB::SELECT('SELECT g.id AS grado_id, CONCAT(g.nombre, " - ", s.nombre) AS grado FROM grado g, seccion s WHERE g.seccion_id = s.id');
                $periodo = \DB::SELECT('SELECT * FROM periodo');

                return response()->json(["view"=>view('matricula.nuevaMatricula',compact('estudiante', 'gradoSeccion', 'periodo'))->render()]);
            }

        }else{
            return response()->json(["view"=>view('notificacion.existe')->render()]);
        }
    }

    public function mostrarCursos(Request $request)
    {
        $cursos = \DB::SELECT('SELECT c.nombre AS curso, CONCAT(e.nombre, " ", e.apellido) AS docente 
                               FROM curso_grado cg, cursos c, grado g, empleado e
                               WHERE cg.cursos_id = c.id AND cg.grado_id = g.id AND cg.empleado_id = e.id AND g.id = "'.$request->grado_id.'"');

        return response()->json(["view"=>view('matricula.listCursos',compact('cursos'))->render()]);
    }

    public function verNivelAcademico(Request $request)
    {
        $grado = \DB::SELECT('SELECT g.* 
                              FROM grado g, nivel_grado ng, nivel n
                              WHERE ng.grado_id = g.id AND ng.nivel_id = n.id AND n.id = "'.$request->nivel.'"');

        return response()->json(["view"=>view('matricula.cbGrado',compact('grado'))->render()]);
    }

    public function verSeccion(Request $request)
    {
        $seccion = \DB::SELECT('SELECT s.* 
                                FROM grado g, seccion s, grado_seccion gs
                                WHERE gs.grado_id = g.id AND gs.seccion_id = s.id AND g.id = "'.$request->grado_id.'"');

        return response()->json(["view"=>view('matricula.cbSeccion',compact('seccion'))->render()]);
    }

    public function busquedaAlumno(Request $request)
    {
        $dato = $request->dato; 
        
        $estudiante = \DB::SELECT('SELECT * 
                                   FROM estudiante 
                                   WHERE CONCAT(nombre, " ", apellido) = "'.$dato.'" OR dni = "'.$dato.'"');

        $grado = \DB::SELECT('SELECT "" AS grado');
        $nivel = \DB::SELECT('SELECT * FROM nivel');

        if ($estudiante == null) {
            $estudiante = \DB::SELECT('SELECT "" AS nombre, "" AS apellido, "" AS dni, "" AS id, "NO EXISTE" AS estado');
        }else{
            $maxMatricula = \DB::SELECT('SELECT MAX(id) AS id 
                                         FROM matricula 
                                         WHERE estudiante_id = "'.$estudiante[0]->id.'"');

            if ($maxMatricula[0]->id == null) {
                $grado = \DB::SELECT('SELECT grado FROM traslado WHERE estudiante_id = "'.$estudiante[0]->id.'"');
            }else {
                $grado = \DB::SELECT('SELECT nombre AS grado FROM matricula m, grado g WHERE m.grado_id = g.id AND m.id = "'.$maxMatricula[0]->id.'"');
            }
            $nivel = \DB::SELECT('SELECT * FROM nivel');

        }

        return response()->json(["view"=>view('matricula.datosAlumno',compact('estudiante', 'grado', 'nivel'))->render(), "estado"=>$estudiante[0]->estado]);
    }

    public function matriculaMasiva ()
    {
        $estudiante = \DB::SELECT('SELECT e.id AS estudiante_id, e.nombre AS nombre, e.apellido AS apellido, e.dni AS dni, g.nombre AS grado, m.id AS matricula_id 
                                   FROM matricula m, estudiante e, grado g
                                   WHERE m.estudiante_id = e.id AND m.grado_id = g.id AND m.estado = "APROBADO"');

        $nivel = \DB::SELECT('SELECT * FROM nivel');

        return view('matricula.matriculaMasiva ', compact('estudiante', 'nivel'));
    }

    public function traslados()
    {

        $traslado = \DB::SELECT('SELECT t.id, t.resolucion, t.url, t.nivel, t.grado, t.anio, t.estado, e.id AS estudiante_id, e.dni, e.nombre, e.apellido, c.id AS colegio_id, c.codModular, c.nombre AS nomColegio
                                 FROM traslado t, estudiante e, colegio c
                                 WHERE e.id = t.estudiante_id AND t.colegio_id = c.id');

        return view('matricula.traslados', compact('traslado'));
    }

    public function buscarAlumnoTraslado(Request $request)
    {
        $dni = $request->dni;

        $alumno = \DB::SELECT('SELECT id, nombre, apellido
                               FROM estudiante 
                               WHERE dni = "'.$dni.'"');

        if ($alumno == null) {
            $alumno = \DB::SELECT('SELECT "NO" AS id, "" AS nombre, "" AS apellido');
        }

        return response()->json(["id" => $alumno[0]->id, "nombre" => $alumno[0]->nombre, "apellido" => $alumno[0]->apellido]);

    }

    public function buscarColegioTraslado(Request $request)
    {
        $codigo = $request->codModular;

        $colegio = \DB::SELECT('SELECT id, codModular, nombre
                                FROM colegio
                                WHERE codModular = "'.$codigo.'"');

        if ($colegio == null) {
            $colegio = \DB::SELECT('SELECT "NO" AS id, "'.$codigo.'" AS codModular, "" AS nombre');
        }

        return response()->json(["id" => $colegio[0]->id, "colegio" => $colegio[0]->nombre, "codModular" => $colegio[0]->codModular]);

    }

    public function guardarColegio(Request $request)
    {
        $codModular = $request->codModular;
        $colegio = $request->colegio;

        $col = new Colegio();
        $col->codModular = $codModular;
        $col->nombre = $colegio;
        if ($col->save()) {
            $colegio = \DB::SELECT('SELECT id, nombre AS colegio, codModular FROM colegio WHERE codModular = "'.$codModular.'"');
        }else{
            $colegio = \DB::SELECT('SELECT "NO" AS id, "" AS colegio, "" AS codModular');
        }

        return response()->json(["id" => $colegio[0]->id, "colegio" => $colegio[0]->colegio, "codModular" => $colegio[0]->codModular]);

    }

    public function mostrarGradoTraslado(Request $request)
    {
        $nivel = $request->nivel;

        if($nivel == "Inicial"){
            return response()->json(["view"=>view('matricula.cbTrasladoInicial')->render()]);
        }else if($nivel == "Primaria"){
            return response()->json(["view"=>view('matricula.cbTrasladoPrinaria')->render()]);
        }else if($nivel == "Secundaria"){
            return response()->json(["view"=>view('matricula.cbTrasladoSecundaria')->render()]);
        }else{

        }

    }

    public function guardarTraslado(Request $request)
    {

        $numResolucion = $request->numResolucion;
        $estudiante_id = $request->estudiante_id;
        $colegio_id = $request->colegio_id;
        $colegio = $request->nomColegio;
        $anio = $request->anio;
        $nivel = $request->nivel;
        $grado = $request->grado;
        $subido="";
        $urlGuardar="";

        if ($request->hasFile('numResolucion')) { 
            $nomResolucion = $numResolucion->getClientOriginalName();
            $extension = $numResolucion->getClientOriginalExtension();
            $nuevoNombre = $nomResolucion;
            $subido = Storage::disk('resolucionTraslado')->put($nuevoNombre, \File::get($numResolucion));
            if($subido){
                $urlGuardar='doc/resolucionTraslado/'.$nuevoNombre;
            }
        }

        $tras = new Traslado();
        $tras->resolucion = $nomResolucion;
        $tras->url = $urlGuardar;
        $tras->nivel = $nivel;
        $tras->grado = $grado;
        $tras->anio = $anio;
        $tras->estado = "ACTIVO";
        $tras->estudiante_id = $estudiante_id;
        $tras->colegio_id = $colegio_id;
        if ($tras->save()) {

            $est = Estudiante::where('id', '=', $estudiante_id)->first();
            $est->estado = "REGISTRADO";
            if ($est->save()) {
                $traslado = \DB::SELECT('SELECT t.id, t.resolucion, t.url, t.nivel, t.grado, t.anio, t.estado, e.id AS estudiante_id, e.dni, e.nombre, e.apellido, c.id AS colegio_id, c.codModular, c.nombre AS nomColegio
                                     FROM traslado t, estudiante e, colegio c
                                     WHERE e.id = t.estudiante_id AND t.colegio_id = c.id');

            return response()->json(["tabTraslado"=>view('matricula.lisTraslado', compact('traslado'))->render()]);
            }
        }
    }

    public function buscarTraslado(Request $request)
    {
        return response()->json(["view"=>view('notificacion.existe')->render(), "view2"=>view('matricula.lisTraslado')->render()]);
    }

    public function nominas()
    {
        $grado = \DB::SELECT('SELECT * FROM grado');
        
        return view('matricula.nominas', compact('grado'));
    }

    public function buscarNomina(Request $request) 
    {
        return response()->json(["view"=>view('notificacion.existe')->render()]);
    }

    public function mostrarSeccion(Request $request)
    {
        $seccion = \DB::SELECT('SELECT s.id AS seccion_id, s.nombre AS seccion FROM seccion s, grado g WHERE g.seccion_id = s.id AND g.id = "'.$request->grado_id.'"');

        return response()->json(["view"=>view('matricula.mostrarSeccion',compact('seccion'))->render()]);
    }

    public function reportes()
    {
        return view('matricula.reportes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mostrarGrado(Request $request)
    {
        $nivel_id = $request->nivel_id;

        $grado = \DB::SELECT('SELECT g.id AS id, g.nombre AS nombre
                              FROM nivel_grado ng, grado g
                              WHERE ng.grado_id = g.id AND ng.nivel_id = "'.$nivel_id.'"');

        return response()->json(["view"=>view('matricula.cbGrado',compact('grado'))->render()]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
