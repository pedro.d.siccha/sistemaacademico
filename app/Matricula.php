<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    protected $table = 'matricula';
    protected $fillable = ['id', 'estudiante_id', 'grado_id', 'periodo_id', 'anioescolar_id'];

    public static function matEst($id){
    	return Matricula::where('estudiante_id', '=', $id) -> get();
    }

    public static function matGra($id){
        return Matricula::where('grado_id', '=', $id) -> get();
    }

    public static function matPer($id){
        return Matricula::where('periodo_id', '=', $id) -> get();
    }

    public static function matAes($id){
        return Matricula::where('anioescolar', '=', $id) -> get();
    }
}
