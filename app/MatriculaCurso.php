<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class MatriculaCurso extends Model
{
    protected $table = 'matricula_curso';
    protected $fillable = ['id', 'detalle', 'cursos_id', 'matricula_id', 'evaluacion_id', 'asistencia_id'];

    public static function mcuCur($id){
    	return MatriculaCurso::where('cursos_id', '=', $id) -> get();
    }

    public static function mcuMat($id){
        return MatriculaCurso::where('matricula_id', '=', $id) -> get();
    }

    public static function mcuEva($id){
        return MatriculaCurso::where('evaluacion_id', '=', $id) -> get();
    }

    public static function mcuAsi($id){
        return MatriculaCurso::where('asistencia_id', '=', $id) -> get();
    }
}
