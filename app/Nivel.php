<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Nivel extends Model
{
    protected $table = 'nivel';
    protected $fillable = ['id', 'nombre', 'detalle', 'anioescolar_id'];

    public static function nivAEsc($id){
    	return Nivel::where('anioescolar_id', '=', $id) -> get();
    }
}
