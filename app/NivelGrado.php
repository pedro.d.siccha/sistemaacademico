<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class NivelGrado extends Model
{
    protected $table = 'nivel_grado';
    protected $fillable = ['id', 'nivel_id', 'grado_id'];
    protected $guarded = ['id'];

    public function nivel()
    {
        return $this->belongsTo(Nivel::class, 'nivel_grado');
    }

    public function grado()
    {
        return $this->belongsTo(Grado::class, 'nivel_grado');
    }

}
