<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Parentezco extends Model
{
    protected $table = 'parentezco';
    protected $fillable = ['id', 'parentezco'];
}
