<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    protected $table = 'periodo';
    protected $fillable = ['id', 'nombre', 'duracuion', 'tipoperiodo_id'];

    public static function perTPe($id){
    	return Periodo::where('tipoperiodo_id', '=', $id) -> get();
    }
}
