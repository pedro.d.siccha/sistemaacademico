<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    protected $table = 'seccion';
    protected $fillable = ['id', 'nombre'];

}
