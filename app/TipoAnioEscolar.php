<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class TipoAnioEscolar extends Model
{
    protected $table = 'tipoanioescolar';
    protected $fillable = ['id', 'tipoanio'];

}
