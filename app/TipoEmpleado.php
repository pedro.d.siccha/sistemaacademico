<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class TipoEmpleado extends Model
{
    protected $table = 'tipo_empleado';
    protected $fillable = ['id', 'nombre', 'detalles'];
}
