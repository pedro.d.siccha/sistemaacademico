<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class TipoPeriodo extends Model
{
    protected $table = 'tipoperiodo';
    protected $fillable = ['id', 'nombre', 'duracion'];

}
