<?php

namespace GestionAcademica;

use Illuminate\Database\Eloquent\Model;

class Traslado extends Model
{
    protected $table = 'traslado';
    protected $fillable = ['id', 'resolucion', 'url', 'nivel', 'grado', 'anio', 'estado', 'estudiante_id', 'colegio_id'];

    public static function traEst($id){
    	return Traslado::where('estudiante_id', '=', $id) -> get();
    }

    public static function traCol($id){
    	return Traslado::where('colegio_id', '=', $id) -> get();
    }

}
