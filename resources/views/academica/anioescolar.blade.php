@extends('layouts.app')
@section('nombrePagina')
 Año Escolar   
@endsection
@section('contenido')
<div class="clearfix"></div>

<div class="col-md-12 col-sm-6 col-xs-12"> 
  <div class="x_panel">
    <div class="x_title">
      <h2>Año Escolar <small>Periodo Escolar</small> <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Crear Nuevo Año Escolar" onclick="nuevoAnio()"><i class="fa fa-plus"></i></button></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content" id="listAnioEscolar">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Código</th>
            <th>Descripcion</th>
            <th>Especialidades</th>
            <th>Fecha de Inicio</th>
            <th>Fecha de Fin</th>
            <th>Estado</th>
            <th>Administración</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($anioEscolar as $ae)
            <tr>
              <th scope="row">{{ $ae->id }}</th>
              <td>{{ $ae->nombre }}</td>
              <td>Ver Especialidades</td>
              <td>{{ $ae->fecinicio }}</td>
              <td>{{ $ae->fecfin }}</td>
              <td><button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="En Proceso" onclick="detalleAnio('{{ $ae->id }}')"><i class="fa fa-check"></i></button></td>
              <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar Año Escolar" onclick="editarAnio('{{ $ae->id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Año Escolar" onclick="eliminarAnio('{{ $ae->id }}')"><i class="fa fa-trash"></i></button><button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Finalizar Año Escolar"><i class="fa fa-close"></i></button></td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>

<div class="modal inmodal fade" id="crearAnioEscolar" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Crear Nuevo Año Escolar</h4>
          </div>
          <div class="modal-body">
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Nombre del Año</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="nomAnio"></div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Descripción</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="descAnio"></div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Fecha de Inicio</label>
              <div class="col-sm-10"><input type="date" class="form-control" id="fecIniAnio"></div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Fecha de Fin</label>
              <div class="col-sm-10"><input type="date" class="form-control" id="fecFinAnio"></div>
            </div>
            <div class="form-group row"><label class="col-sm-2 col-form-label">Tipo de Año Escolar</label>
              <div class="col-sm-10">
                <select class="form-control m-b" name="account" id="tipoAnio">
                  <option>Seleccionar el tipo de año escolar...</option>
                  @foreach ($tipoAnio as $ta)
                  <option value="{{ $ta->id }}">{{ $ta->tipoanio }}</option>
                  @endforeach
                </select>
              </div>
          </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="guardarAnio()">Crear</button>
          </div>
      </div>
  </div>
</div>

@endsection
@section('script')
<script>
function nuevoAnio(){ 
  $('#crearAnioEscolar').modal('show');
}

function guardarAnio(){ 

var nombre = $('#nomAnio').val();
var descripcion = $('#descAnio').val();
var fecinicio = $('#fecIniAnio').val();
var fecfin = $('#fecFinAnio').val();
var tipoAnio = $('#tipoAnio').val();
$('#crearAnioEscolar').modal('hide');

$.post( "{{ Route('crearAnioEscolar') }}", {nombre: nombre, descripcion: descripcion, fecinicio: fecinicio, fecfin: fecfin, tipoAnio: tipoAnio, _token:'{{csrf_token()}}'}).done(function(data) {
                $("#listAnioEscolar").empty();
                $("#listAnioEscolar").html(data.view);

            });

}

function EditarPaciente(){ 

  var nombre = $('#nombre').val();
  var apellido = $('#apellido').val();
  var fecnac = $('#fecnac').val();
  var dni = $('#dni').val();
  var direccion = $('#direccion').val();
  var telefono = $('#telefono').val();
  var lugarnac = $('#lugarnac').val();
  var lugarproc = $('#lugarproc').val();
  var correo = $('#correo').val();
  var ocu = $('#ocu').val();
  var inst = $('#inst').val();
  var eci = $('#eci').val();
  var gnero = $('#gnero').val();
  var idPaciente = $('#idPaciente').val();

  
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection