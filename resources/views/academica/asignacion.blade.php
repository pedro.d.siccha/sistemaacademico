@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')
<div class="clearfix"></div>

<div class="row">

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Asignación Nivel </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Código</th>
              <th>Nivel</th>
              <th>Asignar Grado</th>
              <th>Administración</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($listNivel as $ln)
              <tr>
                <th scope="row" onclick="verGrados('{{ $ln->id }}')">{{ $ln->id }}</th>
                <td  onclick="verGrados('{{ $ln->id }}')">{{ $ln->nombre }}</td>
                <td><button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="ASIGNAR GRADOS HABILITADOS" onclick="mostrarGrados('{{ $ln->id }}', '{{ $ln->nombre }}')"><i class="fa fa-check-square-o"></i></button></td>
                <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="ASIGNAR CURSOS" onclick="asignarCursos('{{ $ln->id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="ELIMINAR"><i class="fa fa-trash"></i></button></td>
              </tr>
            @endforeach
          </tbody>
        </table>
  
      </div>
    </div>
  </div>

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Grados </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="tabGrados">
        
  
      </div>
    </div>
  </div>

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Sección </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="tabSeccion">
        
  
      </div>
    </div>
  </div>

</div>

<!-- Lista de Grados para asignar a los niveles -->

<div class="modal inmodal fade" id="listGrados" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Asignar Cursos <span id="titNivel">Nivel</span></h4>
              <input type="text" hidden id="idNivel">
          </div>
          <div class="modal-body">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th></th>
                  <th>Grado</th>
                </tr>
              </thead>

              <tbody>
                @foreach ($listGrado as $lg)
                  <tr>
                    <td>
                        <input type="checkbox" class="flat chekbox_grado" name="idGrado[]" value="{{ $lg->id }}" id="idGrado" >
                    </td>
                    <td>{{ $lg->nombre }}</td>
                  </tr>    
                @endforeach
              </tbody>
            </table>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="asignarGrado()">Asignar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Grados -->

<!-- Lista de Secciones para asignar a los grados -->

<div class="modal inmodal fade" id="listSeccion" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Asignar Sección <span id="titGrado">Nivel</span></h4>
              <input type="text" hidden id="idGrado">
          </div>
          <div class="modal-body">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th></th>
                  <th>Sección</th>
                </tr>
              </thead>

              <tbody>
                @foreach ($listSeccion as $ls)
                  <tr>
                    <td>
                        <input type="checkbox" class="flat chekbox_seccion" name="idSeccion[]" value="{{ $ls->id }}" id="idSeccion" >
                    </td>
                    <td>{{ $ls->nombre }}</td>
                  </tr>    
                @endforeach
              </tbody>
            </table>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="asignarSeccion()">Asignar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Sección -->

@endsection
@section('script')
<script>

  function mostrarGrados(id, nivel){ 
    document.getElementById("titNivel").innerHTML="" + nivel +"";
    $('#listGrados').modal('show');
    $("#idNivel").val(id);
    
  }

function asignarGrado(){ 

  var nivel_id = $('#idNivel').val();
  var idGrado = [];
  $('.chekbox_grado:checked').each(
  function() {
      idGrado.push($(this).val());
    }
  );

  $.post( "{{ Route('asignarGrado') }}", {nivel_id: nivel_id, idGrado: idGrado, _token:'{{csrf_token()}}'}).done(function(data) {
            $('#listGrados').modal('hide');
            if(data.res == 0){
              swal("ERROR ", "Hubo un problema al registrar los grados \nCodigo de Error: ".data.txtResp, "error");
            }else{
              swal("CORRECTO", "Grados asignados correctamente", "success");
            }

            $("#tabGrados").empty();
            $("#tabGrados").html(data.view);

            });
  
}


function verGrados(idNivel){
  $.post( "{{ Route('verGrados') }}", {idNivel: idNivel, _token:'{{csrf_token()}}'}).done(function(data) {

    $("#tabGrados").empty();
    $("#tabGrados").html(data.view);

    });
}

function verSeccion(idGrado){
  $.post( "{{ Route('verSeccion') }}", {idGrado: idGrado, _token:'{{csrf_token()}}'}).done(function(data) {

    $("#tabSeccion").empty();
    $("#tabSeccion").html(data.view);

    });
}

function mostrarSeccion(id, grado){
    document.getElementById("titGrado").innerHTML="" + grado +"";
    $('#listSeccion').modal('show');
    $("#idGrado").val(id);
}

function asignarSeccion(){ 

  var grado_id = $('#idGrado').val();
  var idSeccion = [];
  $('.chekbox_seccion:checked').each(
  function() {
      idSeccion.push($(this).val());
    }
  );

  $.post( "{{ Route('asignarSeccion') }}", {grado_id: grado_id, idSeccion: idSeccion, _token:'{{csrf_token()}}'}).done(function(data) {
            $('#listSeccion').modal('hide');
            if(data.res == 0){
              swal("ERROR ", "Hubo un problema al registrar las secciones \nCodigo de Error: ".data.txtResp, "error");
            }else{
              swal("CORRECTO", "Secciones asignadas correctamente", "success");
            }

            $("#tabSeccion").empty();
            $("#tabSeccion").html(data.view);

            });
  
}

</script>
@endsection