@extends('layouts.app')
@section('nombrePagina')
 Horarios
@endsection
@section('contenido')

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Nominas <small>Generacion y Envio</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel Educativo <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="nivel_id" class="form-control" required onchange="mostrarGrados()">
                <option value="">Seleccione...</option>
                @foreach ($nivel as $n)
                  <option value="{{ $n->id }}">{{ $n->nombre }}</option>    
                @endforeach
              </select>
            </div>
          </div>
          <div class="ln_solid"></div>

        </form>
      </div>
      
    </div>
    <div id="tabHorario" class="row"></div>
  </div>
</div>

@endsection
@section('script')

<script>

  function mostrarGrados(){ 
    var idNivel = $('#nivel_id').val();

    $.post( "{{ Route('mostrarHorario') }}", { idNivel: idNivel, _token:'{{csrf_token()}}'}).done(function(data) {
      $("#tabHorario").empty();
      $("#tabHorario").html(data.view);
  
    });
  }

function generarHorario(){ 
  $.post( "{{ Route('generarHorario') }}", { _token:'{{csrf_token()}}'}).done(function(data) {
    $("#tabHorario").empty();
    $("#tabHorario").html(data.view);

  });
}

function guardarLunes7(hora, dia){ 

  var curso = $('#lunes7').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMartes7(hora, dia){ 

  var curso = $('#martes7').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMiercoles7(hora, dia){ 

  var curso = $('#miercoles7').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarJueves7(hora, dia){ 

  var curso = $('#jueves7').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarViernes7(hora, dia){ 

  var curso = $('#viernes7').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarLunes8(hora, dia){ 

  var curso = $('#lunes8').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMartes8(hora, dia){ 

  var curso = $('#martes8').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMiercoles8(hora, dia){ 

  var curso = $('#miercoles8').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarJueves8(hora, dia){ 

  var curso = $('#jueves8').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarViernes8(hora, dia){ 

  var curso = $('#viernes8').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarLunes9(hora, dia){ 

  var curso = $('#lunes9').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMartes9(hora, dia){ 

  var curso = $('#martes9').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMiercoles9(hora, dia){ 

  var curso = $('#miercoles9').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarJueves9(hora, dia){ 

  var curso = $('#jueves9').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarViernes9(hora, dia){ 

  var curso = $('#viernes9').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarLunes10(hora, dia){ 

  var curso = $('#lunes10').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMartes10(hora, dia){ 

  var curso = $('#martes10').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMiercoles10(hora, dia){ 

  var curso = $('#miercoles10').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarJueves10(hora, dia){ 

  var curso = $('#jueves10').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarViernes10(hora, dia){ 

  var curso = $('#viernes10').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarLunes11(hora, dia){ 

  var curso = $('#lunes11').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMartes11(hora, dia){ 

  var curso = $('#martes11').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMiercoles11(hora, dia){ 

  var curso = $('#miercoles11').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarJueves11(hora, dia){ 

  var curso = $('#jueves11').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarViernes11(hora, dia){ 

  var curso = $('#viernes11').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarLunes12(hora, dia){ 

  var curso = $('#lunes12').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMartes12(hora, dia){ 

  var curso = $('#martes12').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMiercoles12(hora, dia){ 

  var curso = $('#miercoles12').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarJueves12(hora, dia){ 

  var curso = $('#jueves12').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarViernes12(hora, dia){ 

  var curso = $('#viernes12').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarLunes13(hora, dia){ 

  var curso = $('#lunes13').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMartes13(hora, dia){ 

  var curso = $('#martes13').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarMiercoles13(hora, dia){ 

  var curso = $('#miercoles13').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarJueves13(hora, dia){ 

  var curso = $('#jueves13').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarViernes13(hora, dia){ 

  var curso = $('#viernes13').val();

  toastr.success("Se registro el curso. \nDía: " + dia + ". \nHora: " + hora + ".", "CURSO GUARDADO");
  
}

function guardarHorario(){ 

  var idNivel = $('#nivel_id').val();
  var nivel = "";

  if (idNivel == 14){

    var nivel = "PRIMARIA";

  }else if(idNivel == 15){

    var nivel = "SECUNDARIA";

  }else if (idNivel == 16){

    var nivel = "INICIAL";

  }

  swal("Correcto", "Horario generado correctamente para el nivel " + nivel, "success");

}


</script>
@endsection