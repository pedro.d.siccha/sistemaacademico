<table class="table table-hover">
    <thead>
        <tr>
        <th>Código</th>
        <th>Descripcion</th>
        <th>Especialidades</th>
        <th>Fecha de Inicio</th>
        <th>Fecha de Fin</th>
        <th>Estado</th>
        <th>Administración</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($anioEscolar as $ae)
        <tr>
            <th scope="row">{{ $ae->id }}</th>
            <td>{{ $ae->nombre }}</td>
            <td>Ver Especialidades</td>
            <td>{{ $ae->fecinicio }}</td>
            <td>{{ $ae->fecfin }}</td>
            <td><button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="En Proceso" onclick="detalleAnio('{{ $ae->id }}')"><i class="fa fa-check"></i></button></td>
            <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar Año Escolar" onclick="editarAnio('{{ $ae->id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Año Escolar" onclick="eliminarAnio('{{ $ae->id }}')"><i class="fa fa-trash"></i></button><button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Finalizar Año Escolar"><i class="fa fa-close"></i></button></td>
        </tr>
        @endforeach
    </tbody>
</table>