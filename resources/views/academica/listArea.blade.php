<table class="table table-hover">
    <thead>
      <tr>
        <th>Código</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Administración</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($curso as $c)
        <tr>
          <th scope="row">{{ $c->id }}</th>
          <td>{{ $c->nombre }}</td>
          <td>{{ $c->horaasignada }}</td>
          <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar Área" onclick="editarArea('{{ $c->id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Área" onclick="eliminarArea('{{ $c->id }}')"><i class="fa fa-trash"></i></button></td>
        </tr>    
      @endforeach
      
    </tbody>
  </table>