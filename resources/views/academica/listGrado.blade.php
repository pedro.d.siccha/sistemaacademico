<table class="table table-hover">
    <thead>
        <tr>
        <th>Código</th>
        <th>Descripcion</th>
        <th>Estado</th>
        <th>Administración</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grado as $g)
        <tr>
            <th scope="row">{{ $g->id }}</th>
            <td>{{ $g->nombre }}</td>
            <td><button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="CORRECTO"><i class="fa fa-check"></i></button></td>
            <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="EDITAR" onclick="editarGrado('{{ $g->id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="ELIMINAR" onclick="eliminarGradp('{{ $g->id }}')"><i class="fa fa-trash"></i></button></td>
        </tr>    
        @endforeach
    </tbody>
</table>