<table class="table table-hover">
    <thead>
        <tr>
        <th>Código</th>
        <th>Grado</th>
        <th>Administración</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>A</td>
            <td><button onclick="verHorario()" type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Horario"><i class="fa fa-archive"></i></button></td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>A</td>
            <td><button onclick="verHorario()" type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Horario"><i class="fa fa-archive"></i></button></td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>A</td>
            <td><button onclick="verHorario()" type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Horario"><i class="fa fa-archive"></i></button></td>
        </tr>
    </tbody>
</table>

<script>
    function verHorario(){ 
      $.post( "{{ Route('verHorario') }}", { _token:'{{csrf_token()}}'}).done(function(data) {
        $("#verHorario").empty();
        $("#verHorario").html(data.view);
    
      });
    }
</scrip>