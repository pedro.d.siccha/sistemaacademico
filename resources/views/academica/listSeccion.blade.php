<table class="table table-hover">
  <thead>
    <tr>
      <th>Código</th>
      <th>Sección</th
      <th>Administración</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($seccion as $s)
      <tr>
        <th scope="row">{{ $s->id }}</th>
        <td>{{ $s->nombre }}</td>
        <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="EDITAR" onclick="editarSeccion('{{ $s->id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="ELIMINAR" onclick="eliminarSeccion('{{ $s->id }}')"><i class="fa fa-trash"></i></button></td>
      </tr>    
    @endforeach
  </tbody>
</table>