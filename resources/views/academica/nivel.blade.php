@extends('layouts.app')
@section('nombrePagina')
 Nivel Educativo
@endsection
@section('contenido')
<div class="clearfix"></div>

<div class="col-md-12 col-sm-6 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Nivel Educativo <small>Periodo Escolar</small> <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="AGREGAR" onclick="crearNivel()"><i class="fa fa-plus"></i></button></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content" id="listNivel">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Código</th>
            <th>Nivel</th>
            <th>Estado</th>
            <th>Administración</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($nivel as $n)
            <tr>
              <th scope="row">{{ $n->id }}</th>
              <td>{{ $n->nombre }}</td>
              <td><button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="CORRECTO"><i class="fa fa-check"></i></button></td>
              <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="EDITAR" onclick="editarGrado('{{ $n->id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="ELIMINAR" onclick="eliminarGradp('{{ $n->id }}')"><i class="fa fa-trash"></i></button></td>
            </tr>    
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>


<div class="modal inmodal fade" id="crearNivel" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Crear Nuevo Grado</h4>
          </div>
          <div class="modal-body">
            <div class="form-group row"><label class="col-sm-2 col-form-label">Año Escolar</label>
                <div class="col-sm-10">
                  <select class="form-control m-b" name="account" id="anioescolar_id">
                    <option>Seleccionar Año Escolar...</option>
                    @foreach ($anioescolar as $ae)
                    <option value="{{ $ae->id }}">{{ $ae->nombre }}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Nivel</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="nomNivel"></div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Detalle</label>
                <div class="col-sm-10"><input type="text" class="form-control" id="detNivel"></div>
            </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="guardarNivel()">Crear</button>
          </div>
      </div>
  </div>
</div>

@endsection
@section('script')
<script>
function crearNivel() {
  $('#crearNivel').modal('show');
}

function guardarNivel(){ 
  
  var anioescolar_id = $('#anioescolar_id').val();
  var nombre = $('#nomNivel').val();
  var detalle = $('#detNivel').val();

  
  $.post( "{{ Route('guardarNivel') }}", {anioescolar_id: anioescolar_id, nombre: nombre, detalle: detalle, _token:'{{csrf_token()}}'}).done(function(data) {
            
            $("#listNivel").empty();
            $("#listNivel").html(data.view);
            $('#crearNivel').modal('hide');

            if(data.res == 1){
                swal("CORRECTO", "Nivel Guardado Correctamente", "success");
            }else{
                swal("ERROR", "Error al guardar el nivel", "error");
            }

            });
}

function EditarPaciente(){ 

  var nombre = $('#nombre').val();
  var apellido = $('#apellido').val();
  var fecnac = $('#fecnac').val();
  var dni = $('#dni').val();
  var direccion = $('#direccion').val();
  var telefono = $('#telefono').val();
  var lugarnac = $('#lugarnac').val();
  var lugarproc = $('#lugarproc').val();
  var correo = $('#correo').val();
  var ocu = $('#ocu').val();
  var inst = $('#inst').val();
  var eci = $('#eci').val();
  var gnero = $('#gnero').val();
  var idPaciente = $('#idPaciente').val();

  
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection