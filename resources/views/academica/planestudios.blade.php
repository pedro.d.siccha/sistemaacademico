@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')

<div class="clearfix"></div>

<div class="col-md-12 col-sm-6 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Nivel Educativo </h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">

      <!-- start accordion -->
      <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach ($nivel as $n)
          <div class="panel">
            <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $n->id }}" aria-expanded="false" aria-controls="collapseTwo" onclick="mostrarGrado('{{ $n->id }}')">
              <h4 class="panel-title">Nivel: {{ $n->nombre }}</h4>
            </a>
            <div id="collapse{{ $n->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body" id="tabGrado-{{ $n->id }}">
                
                
              </div>
            </div>
          </div>    
        @endforeach
        
      </div>
      <!-- end of accordion -->


    </div>
  </div>
</div>

<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-bars"></i> Niveles </h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      
      <div class="col-xs-3">
        <!-- required for floating -->
        <!-- Nav tabs -->
        <ul class="nav nav-tabs tabs-left">
          @foreach ($nivel as $n)
          <li onclick="verGrados('{{ $n->id }}')"><a href="#tabNivel" data-toggle="tab">{{ $n->nombre }}</a></li>
          @endforeach
        </ul>
      </div>

      <div class="col-xs-9">
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="tabNivel">
            
            
          </div>
        </div>
      </div>

      <div class="clearfix"></div>

    </div>
  </div>
</div>


<div class="clearfix"></div>

<div class="col-md-12 col-sm-6 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Plan de Estudios <small>Periodo Escolar</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content" id="tabDocCurso">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Foto</th>
            <th>Docente</th>
            <th>Curso</th>
            <th>Grado</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($docenteCurso as $dc)
          <tr>  
            <th><img src="{{ $dc->foto }}" alt="..." style="height: 3%;"></th>
            <td>{{ $dc->nombre }} {{ $dc->apellido }}</td>
            <td>{{ $dc->cursos }}</td>
            <td>{{ $dc->grado }} {{ $dc->nivel }}</td>
          </tr>    
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>

<!-- Lista de Cursos para asignar a los niveles -->

<div class="modal inmodal fade" id="listCursos" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Asignar Cursos <span id="titNivel">Nivel</span> - <span id="titGrado">Grado</span></h4>
              <input type="text" hidden id="idNivel">
              <input type="text" hidden id="idGrado">
          </div>
          <div class="modal-body">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th></th>
                  <th>Cursos</th>
                </tr>
              </thead>

              <tbody>
                @foreach ($curso as $c)
                  <tr>
                    <td>
                        <input type="checkbox" class="flat chekbox_curso" name="idCursos[]" value="{{ $c->id }}" id="idCursos" >
                    </td>
                    <td>{{ $c->nombre }}</td>
                  </tr>    
                @endforeach
              </tbody>
            </table>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="asignarCursos()">Asignar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Cursos -->

<!-- Lista de Docentes para asignar a los Cursos -->

<div class="modal fade" id="listDocentes" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Asignar Docentes <span id="titNivel">Nivel</span> - <span id="titGrado">Grado</span> - <span id="titCurso">Curso</span></h4>
              <input type="text" hidden id="idCursoGrado">
          </div>
          <div class="modal-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="DNI Docente" id="dniDocente">
              <span class="input-group-btn">
                <button type="button" class="btn btn-primary" onclick="buscarDocente()"><i class="fa fa-search"></i></button>
              </span>
            </div>
            <div id="tabPerfilDocente" class="input-group">
              
            </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Docentes -->

@endsection
@section('script')
<script>
function mostrarGrado(idNivel){ 
  
  $.post( "{{ Route('mostrarGradoPlanEstudio') }}", {idNivel: idNivel, _token:'{{csrf_token()}}'}).done(function(data) {
    $("#tabGrado-"+data.idNivel).empty();
    $("#tabGrado-"+data.idNivel).html(data.view);

  });

}

function mostrarCursos(nivelGrado_id, nivel_id, grado_id, nivel, grado){ 

  document.getElementById("titNivel").innerHTML="" + nivel +"";
  document.getElementById("titGrado").innerHTML="" + grado +"";
  $("#idNivel").val(nivel_id);
  $("#idGrado").val(grado_id);
  $('#listCursos').modal('show');
  
}

function verGrados(id){
  $.post( "{{ Route('verGradosDocente') }}", {idNivel: id, _token:'{{csrf_token()}}'}).done(function(data) {
      $("#tabNivel").empty();
      $("#tabNivel").html(data.view);
  });
}

function verCursos(id){
  $.post( "{{ Route('verCursosDocente') }}", {idNivelGrado: id, _token:'{{csrf_token()}}'}).done(function(data) {
      $("#tabCursos").empty();
      $("#tabCursos").html(data.view);
  }); 
}

function asignarCursos(){ 

  var grado_id = $('#idGrado').val();
  var idCursos = [];
  $('.chekbox_curso:checked').each(
  function() {
      idCursos.push($(this).val());
    }
  );

  $.post( "{{ Route('asignarCursos') }}", {grado_id: grado_id, idCursos: idCursos, _token:'{{csrf_token()}}'}).done(function(data) {
            $('#listCursos').modal('hide');
            if(data.res == 0){
              swal("ERROR ", "Hubo un problema al registrar los cursos \nCodigo de Error: ".data.txtResp, "error");
            }else{
              swal("CORRECTO", "Cursos asignados correctamente", "success");
            }
/*
            $("#tabGrados").empty();
            $("#tabGrados").html(data.view);
*/

        });

}

function verDocentes(id, nivel, grado, curso){
  $("#idCursoGrado").val(id);
  document.getElementById("titNivel").innerHTML="" + nivel +"";
  document.getElementById("titGrado").innerHTML="" + grado +"";
  document.getElementById("titCurso").innerHTML="" + curso +"";
  $('#listDocentes').modal('show');
}

function buscarDocente()
{
  var dniDocente = $('#dniDocente').val();
  $.post( "{{ Route('verDocenteDNI') }}", {dniDocente: dniDocente, _token:'{{csrf_token()}}'}).done(function(data) {
    if(data.res == 0){
      swal("ATENCIÓN", "Verifique si el DNI fué correctamente ingresado o registre al nuevo docente", "info");
    }else{
      $("#tabPerfilDocente").empty();
      $("#tabPerfilDocente").html(data.view);
    }
  }); 
}

function asignarDocente(id)
{
  var idDocente = id;
  var cursoGrado_id = $('#idCursoGrado').val();
  
  $.post( "{{ Route('asignarDocente') }}", {idDocente: idDocente, cursoGrado_id: cursoGrado_id, _token:'{{csrf_token()}}'}).done(function(data) {
    $("#tabDocCurso").empty();
    $("#tabDocCurso").html(data.view);

    if(data.res != 0){
      $('#listDocentes').modal('hide');
      swal("CORRECTO", "El docente fue asignado correctamente", "success");
    }else{
      swal("Error", "Código de error: ".data.resText, "error");
    }

  });
  
}


</script>
@endsection