@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')
<div class="clearfix"></div>

<div class="col-md-12 col-sm-6 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Sección Escolar <small>Periodo Escolar</small> <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="AGREGAR" onclick="crearSeccion()"><i class="fa fa-plus"></i></button></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content" id="listSeccion">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Código</th>
            <th>Sección</th
            <th>Administración</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($seccion as $s)
            <tr>
              <th scope="row">{{ $s->id }}</th>
              <td>{{ $s->nombre }}</td>
              <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="EDITAR" onclick="editarSeccion('{{ $s->id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="ELIMINAR" onclick="eliminarSeccion('{{ $s->id }}')"><i class="fa fa-trash"></i></button></td>
            </tr>    
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>

<div class="modal inmodal fade" id="crearSeccion" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Crear Nueva Seccion</h4>
          </div>
          <div class="modal-body">
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Seccion</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="nomSeccion"></div>
            </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="guardarSeccion()">Crear</button>
          </div>
      </div>
  </div>
</div>

@endsection
@section('script')
<script>

function crearSeccion(){
  $('#crearSeccion').modal('show');
}

function guardarSeccion(){ 

  var nombre = $('#nomSeccion').val();
  
  $.post( "{{ Route('crearSeccion') }}", {nombre: nombre, _token:'{{csrf_token()}}'}).done(function(data) {
                $('#crearSeccion').modal('hide');
                $("#listSeccion").empty();
                $("#listSeccion").html(data.view);

                if(data.res == 1){
                  swal("CORRECTO", "La sección se guardó correctamente", "success");
                }else{
                  swal("ERROR", "Hubo un problema al guardar la sección", "error");
                }

            });
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection