<table class="table table-hover">
    <thead>
      <tr>
        <th>Foto</th>
        <th>Docente</th>
        <th>Curso</th>
        <th>Grado</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($docenteCurso as $dc)
      <tr>  
        <th><img src="{{ $dc->foto }}" alt="..." style="height: 3%;"></th>
        <td>{{ $dc->nombre }} {{ $dc->apellido }}</td>
        <td>{{ $dc->cursos }}</td>
        <td>{{ $dc->grado }} {{ $dc->nivel }}</td>
      </tr>    
      @endforeach
    </tbody>
  </table>