<p><strong>GRADO</strong></p>
                
<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Grado</th>
            <th>Asignar Cursos</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grado as $g)
            <tr>
                <th scope="row">{{ $g->nivelGrado_id }}</th>
                <td>{{ $g->grado }}</td>
                <td>
                    <button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="ASIGNAR CURSOS" onclick="mostrarCursos('{{ $g->grado_id }}', '{{ $g->nivel_id }}', '{{ $g->nivelGrado_id }}', '{{ $g->nivel }}', '{{ $g->grado }}')">
                        <i class="fa fa-check-square-o"></i>
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>