<table class="table table-hover">
    <thead>
      <tr>
        <th>Código</th>
        <th>Sección</th>
        <th>Administración</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($gradoSeccion as $gs)
        <tr>
          <th scope="row">{{ $gs->gradoSeccion_id }}</th>
          <td>{{ $gs->seccion }}</td>
          <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="ASIGNAR CURSOS" onclick="asignarCursos('{{ $gs->gradoSeccion_id }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="ELIMINAR"><i class="fa fa-trash"></i></button></td>
        </tr>
      @endforeach
    </tbody>
</table>