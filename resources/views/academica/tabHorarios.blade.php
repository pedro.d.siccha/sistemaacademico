@foreach ($grado as $g)
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>{{ $g->grado }}</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table class="table table-bordered">
          <thead>
            <tr>
              <th>HORA</th>
              <th>Lunes</th>
              <th>Martes</th>
              <th>Miercoles</th>
              <th>Jueves</th>
              <th>Viernes</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">7:00</th>
              <td>
                <select class="form-control" required id="lunes7" onchange="guardarLunes7('7', 'Lunes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="martes7" onchange="guardarMartes7('7', 'Martes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="miercoles7" onchange="guardarMiercoles7('7', 'Miercoles')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="jueves7" onchange="guardarJueves7('7', 'Jueves')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="viernes7" onchange="guardarViernes7('7', 'Viernes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">8:00</th>
              <td>
                <select class="form-control" required id="lunes8" onchange="guardarLunes8('8', 'Lunes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="martes8" onchange="guardarMartes8('8', 'Martes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="miercoles8" onchange="guardarMiercoles8('8', 'Miercoles')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="jueves8" onchange="guardarJueves8('8', 'Jueves')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="viernes8" onchange="guardarViernes8('8', 'Viernes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">9:00</th>
              <td>
                <select class="form-control" required id="lunes9" onchange="guardarLunes9('9', 'Lunes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="martes9" onchange="guardarMartes9('9', 'Martes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="miercoles9" onchange="guardarMiercoles9('9', 'Miercoles')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="jueves9" onchange="guardarJueves9('9', 'Jueves')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
              <td>
                <select class="form-control" required id="viernes9" onchange="guardarViernes9('9', 'Viernes')">
                    <option value="">Cursos...</option>
                    @foreach ($cursos as $c)
                        <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                    @endforeach
                </select>
              </td>
            </tr>
            <tr>
                <th scope="row">10:00</th>
                <td>
                    <select class="form-control" required id="lunes10" onchange="guardarLunes10('10', 'Lunes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="martes10" onchange="guardarMartes10('10', 'Martes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="miercoles10" onchange="guardarMiercoles10('10', 'Miercoles')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="jueves10" onchange="guardarJueves10('10', 'Jueves')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="viernes10" onchange="guardarViernes10('10', 'Viernes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <th scope="row">RECREO</th>
                <th scope="row">RECREO</th>
                <th scope="row">RECREO</th>
                <th scope="row">RECREO</th>
                <th scope="row">RECREO</th>
                <th scope="row">RECREO</th>
            </tr>
            <tr>
                <th scope="row">11:00</th>
                <td>
                    <select class="form-control" required id="lunes11" onchange="guardarLunes11('11', 'Lunes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="martes11" onchange="guardarMartes11('11', 'Martes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="miercoles11" onchange="guardarMiercoles11('11', 'Miercoles')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="jueves11" onchange="guardarJueves11('11', 'Jueves')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="viernes11" onchange="guardarViernes11('11', 'Viernes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <th scope="row">12:00</th>
                <td>
                    <select class="form-control" required id="lunes12" onchange="guardarLunes12('12', 'Lunes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="martes12" onchange="guardarMartes12('12', 'Martes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="miercoles12" onchange="guardarMiercoles12('12', 'Miercoles')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="jueves12" onchange="guardarJueves12('12', 'Jueves')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="viernes12" onchange="guardarViernes12('12', 'Viernes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <th scope="row">13:00</th>
                <td>
                    <select class="form-control" required id="lunes13" onchange="guardarLunes13('13', 'Lunes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="martes13" onchange="guardarMartes13('13', 'Martes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="miercoles13" onchange="guardarMiercoles13('13', 'Miercoles')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="jueves13" onchange="guardarJueves13('13', 'Jueves')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control" required id="viernes13" onchange="guardarViernes13('13', 'Viernes')">
                        <option value="">Cursos...</option>
                        @foreach ($cursos as $c)
                            <option value="{{ $c->cursoGrado_id }}">{{ $c->curso }}</option>    
                        @endforeach
                    </select>
                </td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>
  </div>    
@endforeach
<div class="form-group">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-danger">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="guardarHorario()">Guardar</button>
    </div>
</div>

