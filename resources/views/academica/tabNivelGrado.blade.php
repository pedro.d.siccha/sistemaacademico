<table class="table table-hover">
    <thead>
      <tr>
        <th>Código</th>
        <th>Grado</th>
        <th>Asignar Sección</th>
        <th>Administración</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($nivelGrado as $ng)
        <tr>
          <th scope="row" onclick="verSeccion('{{ $ng->idGrado }}')">{{ $ng->idNivelGrado }}</th>
          <td onclick="verSeccion('{{ $ng->idGrado }}')">{{ $ng->grado }}</td>
          <td><button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="ASIGNAR" onclick="mostrarSeccion('{{ $ng->idGrado }}', '{{ $ng->grado }}')"><i class="fa fa-check-square-o"></i></button></td>
          <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="ASIGNAR CURSOS" onclick="asignarCursos('{{ $ng->idNivelGrado }}')"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="ELIMINAR"><i class="fa fa-trash"></i></button></td>
        </tr>
      @endforeach
    </tbody>
</table>