@foreach ($docente as $d)  

<div class="col-md-3 col-xs-12 widget widget_tally_box">
    <div class="x_panel fixed_height_390">
      <div class="x_content">

        <div class="flex">
          <ul class="list-inline widget_profile_box">
            <li>
              <a>
              </a>
            </li>
            <li>
              <img src="{{ $d->foto }}" alt="..." class="img-circle profile_img">
            </li>
            <li>
              <a>
              </a>
            </li>
          </ul>
        </div>

        <h3 class="name">{{ $d->nombre }} {{ $d->apellido }}</h3>

        <div class="flex">
          <ul class="list-inline count2">
            <li>
              <h3>DNI</h3>
              <span>{{ $d->dni }}</span>
            </li>
            <li>
              <h3> - </h3>
              <span> - </span>
            </li>
            <li>
              <h3>Telefono</h3>
              <span>{{ $d->telefono }}</span>
            </li>
          </ul>
        </div>
        <p>
          TITULO: {{ $d->titulo }}
        </p>
        <div class="col-xs-12 col-sm-12 emphasis">
            <button type="button" class="btn btn-primary btn-xs" onclick="asignarDocente('{{ $d->docente_id }}')">
              <i class="fa fa-check"> </i> ASIGNAR
            </button>
          </div>
      </div>
    </div>
  </div>
@endforeach<div class="col-md-12 col-sm-12 col-xs-12 profile_details">