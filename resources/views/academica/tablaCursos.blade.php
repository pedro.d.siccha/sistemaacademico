<div class="x_content">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th>Cursos</th>
          <th>Docente</th>
          <th>Estado</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($cursoGrado as $cg)    
            <tr>
                <th scope="row">{{ $cg->cursos_id }}</th>
                <td>{{ $cg->cursos }}</td>
                <td>{{ $cg->docente_id }}</td>
                <?php
                    $boton = "";
                    $action = "";
                    $funcion = "";

                    if($cg->docente_id != null){
                        $boton = "btn-success";
                        $action = "fa-check";
                        $funcion = "";
                    }else{
                        $boton = "btn-danger";
                        $action = "fa-plus";
                        $funcion = "verDocentes(' $cg->cursoGrado_id ', ' $cg->nivel ', ' $cg->grado ', ' $cg->cursos ')";
                    }
                ?>
                <td>
                    <button type="button" class="btn btn-round {{ $boton }}" onclick="{{ $funcion }}"><i class="fa {{ $action }}"></i></button>
                </td>
            </tr>
          @endforeach
      </tbody>
    </table>

  </div>