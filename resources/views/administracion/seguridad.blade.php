@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')

<div class="" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Seguridad</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Buscar...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Roles <small>Tipos de usuarios</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Roles</th>
                <th>Descripcion</th>
                <th>Gestionar</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>Administrador</td>
                <td>Administrador</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>Docente</td>
                <td>Docente</td>
                <td>@fat</td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td>Padre</td>
                <td>Padre</td>
                <td>@twitter</td>
              </tr>
              <tr>
                <th scope="row">4</th>
                <td>Alumnos</td>
                <td>Alumnos</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Table de permisos </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">

          <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action">
              <thead>
                <tr class="headings">
                  <th>
                    <input type="checkbox" id="check-all" class="flat">
                  </th>
                  <th class="column-title"># </th>
                  <th class="column-title">Nombre </th>
                  <th class="column-title">Slug </th>
                  <th class="column-title">Descripcion </th>
                </tr>
              </thead>

              <tbody>
                <tr class="even pointer">
                  <td class="a-center ">
                    <input type="checkbox" class="flat" name="table_records">
                  </td>
                  <td class=" ">121000040</td>
                  <td class=" ">Inicio </td>
                  <td class=" ">Inicio</td>
                  <td class=" ">Inicio</td>
                </tr>
                <tr class="odd pointer">
                  <td class="a-center ">
                    <input type="checkbox" class="flat" name="table_records">
                  </td>
                  <td class=" ">121000039</td>
                  <td class=" ">Matricula</td>
                  <td class=" ">Matricula</td>
                  <td class=" ">Matricula</td>
                </tr>
                <tr class="even pointer">
                  <td class="a-center ">
                    <input type="checkbox" class="flat" name="table_records">
                  </td>
                  <td class=" ">121000038</td>
                  <td class=" ">Academico</td>
                  <td class=" ">Academico</td>
                  <td class=" ">Academico</td>
                </tr>
                <tr class="odd pointer">
                  <td class="a-center ">
                    <input type="checkbox" class="flat" name="table_records">
                  </td>
                  <td class=" ">121000037</td>
                  <td class=" ">Alumnos</td>
                  <td class=" ">Alumnos</td>
                  <td class=" ">Alumnos</td>
                </tr>
              </tbody>
            </table>
          </div>
    
  
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
@section('script')
<script>
function CargarPaciente(nom,ape,fecnac,dni,direccion,telefono,lugarnac,lugarproc,correo,ocupacion,instruccion,estadocivil,genero, idPaciente){ 
  $('#editPaciente').modal('show');
  
  $("#nombre").val(nom);
  $("#apellido").val(ape);
  $("#fecnac").val(fecnac);
  $("#dni").val(dni);
  $("#direccion").val(direccion);
  $("#telefono").val(telefono);
  $("#lugarnac").val(lugarnac);
  $("#lugarproc").val(lugarproc);
  $("#correo").val(correo);
  $("#ocu").val(ocupacion);
  $("#inst").val(instruccion);
  $("#eci").val(estadocivil);
  $("#gnero").val(genero);
  $("#idPaciente").val(idPaciente);
}

function EditarPaciente(){ 

  var nombre = $('#nombre').val();
  var apellido = $('#apellido').val();
  var fecnac = $('#fecnac').val();
  var dni = $('#dni').val();
  var direccion = $('#direccion').val();
  var telefono = $('#telefono').val();
  var lugarnac = $('#lugarnac').val();
  var lugarproc = $('#lugarproc').val();
  var correo = $('#correo').val();
  var ocu = $('#ocu').val();
  var inst = $('#inst').val();
  var eci = $('#eci').val();
  var gnero = $('#gnero').val();
  var idPaciente = $('#idPaciente').val();

  
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection