@extends('layouts.app')
@section('nombrePagina')
 Ajustes Básicos
@endsection
@section('contenido')

<div class="clearfix"></div>

<div class="row">

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tipo de Año Escolar </h2><button type="button" class="btn btn-default" onclick="agregarTAnioEscolar()"><i class="fa fa-plus"></i></button>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
              <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content" id="tabTAnio">
      
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Tipo de Año Escolar</th>
                    <th>Username</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($tipoanioescolar as $tae)
                <tr>
                    <th scope="row">{{ $tae->id }}</th>
                    <td>{{ $tae->tipoanio }}</td>
                    <td>@mdo</td>
                </tr>
                @endforeach
                </tbody>
              </table>
      
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tipo de Periodod </h2><button type="button" class="btn btn-default" onclick="agregarTPeriodo()"><i class="fa fa-plus"></i></button>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
              <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content" id="tabTPeriodo">
      
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Periodo</th>
                    <th>Duración</th>
                    <th>Username</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($tipoperiodo as $tp)
                <tr>
                    <th scope="row">{{ $tp->id }}</th>
                    <td>{{ $tp->nombre }}</td>
                    <td>{{ $tp->duracion }}</td>
                    <td>@mdo</td>
                </tr>
                @endforeach
                </tbody>
              </table>
      
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tipo de Empleado </h2><button type="button" class="btn btn-default" onclick="agregarTEmpleado()"><i class="fa fa-plus"></i></button>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
              <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content" id="tabTEmpleado">
      
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Tipo de Empleado</th>
                    <th>Detalles</th>
                    <th>Username</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($tipoempleado as $te)    
                <tr>
                  <th scope="row">{{ $te->id }}</th>
                  <td>{{ $te->nombre }}</td>
                  <td>{{ $te->detalles }}</td>
                  <td>@mdo</td>
                </tr>
                @endforeach
                </tbody>
              </table>
      
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Parentezco </h2><button type="button" class="btn btn-default" onclick="agregarParentezco()"><i class="fa fa-plus"></i></button>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
              <li><a class="close-link"><i class="fa fa-close"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content" id="tabParentezco">
      
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Parentezco</th>
                    <th>Username</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($parentezco as $p)    
                <tr>
                  <th scope="row">{{ $p->id }}</th>
                  <td>{{ $p->parentezco }}</td>
                  <td>@mdo</td>
                </tr>
                @endforeach
                </tbody>
              </table>
      
          </div>
        </div>
      </div>

</div>

<!-- Agregar Tipo de Año Escolar -->

<div class="modal inmodal fade" id="modalAnioEscolar" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Agregar Tipo de Año Escolar</h4>
          </div>
          <div class="modal-body">
              <input type="text" class="form-control" placeholder="Tipo de Año Escolar" id="anioEscolar">
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="guardarTAnio()">Guardar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Agregar -->

<!-- Agregar Tipo de Periodo -->

<div class="modal inmodal fade" id="modalPeriodo" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Agregar Tipo de Periodo</h4>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" placeholder="Tipo de Periodo" id="tipoPeriodo">
            <br>
            <input type="text" class="form-control" placeholder="Duración" id="duracion">
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="guardarTPeriodo()">Guardar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Tipo Periodo -->

<!-- Agregar Tipo Empleado -->

<div class="modal inmodal fade" id="modalEmpleado" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Agregar Tipo de Empleado</h4>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" placeholder="Tipo de Empleado" id="tipoEmpleado">
            <br>
            <input type="text" class="form-control" placeholder="Detalles" id="detalles">
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="guardarTEmpleado()">Guardar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Agregar -->

<!-- Agregar Tipo Empleado -->

<div class="modal inmodal fade" id="modalParentezco" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Agregar Parentezco</h4>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" placeholder="Parentezco" id="nomParentezco">
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="guardarParentezco()">Guardar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Agregar -->

@endsection
@section('script')
<script>

    function agregarTAnioEscolar(){
        $('#modalAnioEscolar').modal('show');
    }

    function guardarTAnio(){
        var tipoAnioEscolar = $('#anioEscolar').val();

        $.post( "{{ Route('guardarTAnio') }}", {tipoAnioEscolar: tipoAnioEscolar, _token:'{{csrf_token()}}'}).done(function(data) {
            $("#tabTAnio").empty();
            $("#tabTAnio").html(data.view);

            $('#modalAnioEscolar').modal('hide');
            if(data.res == 0){
              swal("ERROR ", "Hubo un problema al registrar el tipo de año escolar \nCodigo de Error: ".data.txtResp, "error");
            }else{
              swal("CORRECTO", "El año escolar fue registrado correctamente", "success");
            }

        });
    }

    function agregarTPeriodo(){
        $('#modalPeriodo').modal('show');
    }

    function guardarTPeriodo(){
        var tipoPeriodo = $('#tipoPeriodo').val();
        var duracion = $('#duracion').val();

        $.post( "{{ Route('guardarTPeriodo') }}", {tipoPeriodo: tipoPeriodo, duracion: duracion, _token:'{{csrf_token()}}'}).done(function(data) {
            $("#tabTPeriodo").empty();
            $("#tabTPeriodo").html(data.view);

            $('#modalPeriodo').modal('hide');
            if(data.res == 0){
              swal("ERROR ", "Hubo un problema al registrar el tipo de periodo \nCodigo de Error: ".data.txtResp, "error");
            }else{
              swal("CORRECTO", "El tipo de periodo fué registrado correctamente", "success");
            }

        });
    }

    function agregarTEmpleado(){
        $('#modalEmpleado').modal('show');
    }

    function guardarTEmpleado(){
        var tipoEmpleado = $('#tipoEmpleado').val();
        var detalles = $('#detalles').val();

        $.post( "{{ Route('guardarTEmpleado') }}", {tipoEmpleado: tipoEmpleado, detalles: detalles, _token:'{{csrf_token()}}'}).done(function(data) {
            $("#tabTEmpleado").empty();
            $("#tabTEmpleado").html(data.view);

            $('#modalEmpleado').modal('hide');
            if(data.res == 0){
              swal("ERROR ", "Hubo un problema al registrar el tipo de empleado \nCodigo de Error: ".data.txtResp, "error");
            }else{
              swal("CORRECTO", "El tipo de empleado fué registrado correctamente", "success");
            }

        });
    }

    function agregarParentezco(){
      $('#modalParentezco').modal('show');
    }

    function guardarParentezco(){
      var parentezco = $('#nomParentezco').val();

      $.post( "{{ Route('guardarParentezco') }}", {parentezco: parentezco, _token:'{{csrf_token()}}'}).done(function(data) {
          $("#tabParentezco").empty();
          $("#tabParentezco").html(data.view);

          $('#modalParentezco').modal('hide');
          if(data.res == 0){
            swal("ERROR ", "Hubo un problema al registrar el parentezco \nCodigo de Error: ".data.txtResp, "error");
          }else{
            swal("CORRECTO", "El parentezco fué registrado correctamente", "success");
          }

      });
    }

</script>
@endsection