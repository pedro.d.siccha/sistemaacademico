<table class="table table-hover">
    <thead>
      <tr>
        <th>Foto</th>
        <th>Nombre</th>
        <th>DNI</th>
        <th>Telefono</th>
        <th>Dirección</th>
        <th>Usuario</th>
        <th>Título</th>
        <th>Gestión</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($docente as $d)
      <tr>
        <th scope="row">{{ $d->docente_id }}</th>
        <td>{{ $d->nombre }} {{ $d->apellido }}</td>
        <td>{{ $d->dni }}</td>
        <td>{{ $d->telefono }}</td>
        <td>{{ $d->direccion }}</td>
        <td>{{ $d->usuario }}</td>
        <td>{{ $d->titulo }}</td>
        <td>@mdo</td>
      </tr>
      @endforeach
    </tbody>
  </table>