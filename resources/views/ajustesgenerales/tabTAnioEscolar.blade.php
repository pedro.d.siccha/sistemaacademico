<table class="table table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Tipo de Año Escolar</th>
        <th>Username</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($tipoanioescolar as $tae)
    <tr>
        <th scope="row">{{ $tae->id }}</th>
        <td>{{ $tae->tipoanio }}</td>
        <td>@mdo</td>
    </tr>
    @endforeach
    </tbody>
  </table>