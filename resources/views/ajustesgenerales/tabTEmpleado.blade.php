<table class="table table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Tipo de Empleado</th>
        <th>Detalles</th>
        <th>Username</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($tipoempleado as $te)    
    <tr>
      <th scope="row">{{ $te->id }}</th>
      <td>{{ $te->nombre }}</td>
      <td>{{ $te->detalles }}</td>
      <td>@mdo</td>
    </tr>
    @endforeach
    </tbody>
  </table>