<table class="table table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Periodo</th>
        <th>Duración</th>
        <th>Username</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($tipoperiodo as $tp)
    <tr>
        <th scope="row">{{ $tp->id }}</th>
        <td>{{ $tp->nombre }}</td>
        <td>{{ $tp->duracion }}</td>
        <td>@mdo</td>
    </tr>
    @endforeach
    </tbody>
  </table>