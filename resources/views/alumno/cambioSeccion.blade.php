@extends('layouts.app')
@section('nombrePagina')
 Año Escolar   
@endsection
@section('contenido')

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Cambio de Sección <small>Filtros de Busqueda</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="cbGrado" class="form-control" required>
                <option value="">Seleccione..</option>
                <option value="net">PRIMERO</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Seccion <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="cbSeccion" class="form-control" required onchange="mostrarAlumnos()">
                <option value="">Seleccione..</option>
                <option value="press">A</option>
                <option value="net">B</option>
            </div>
          </div>

        </form>
        <div id="tabAlumno"></div>
      </div>
    </div>
  </div>
</div>




<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
<div class="x_title">
    <h2><button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="CAMBIAR"><i class="fa fa-edi"></i></button></h2>
    <ul class="nav navbar-right panel_toolbox">
    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <ul class="dropdown-menu" role="menu">
        <li><a href="#">Settings 1</a>
        </li>
        <li><a href="#">Settings 2</a>
        </li>
        </ul>
    </li>
    <li><a class="close-link"><i class="fa fa-close"></i></a>
    </li>
    </ul>
    <div class="clearfix"></div>
</div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
                <tr>
                <th>DNI</th>
                <th>Estudiante</th>
                <th>Grado Origen</th>
                <th>Seccion Origen</th>
                <th>Fecha Matricula</th>
                <th>Grado Destino</th>
                <th>Seccion Destino</th>
                <th>Estado</th>
                <th>Resolución</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>

@endsection
@section('script')
<script>

function mostrarAlumnos(){


  var grado = $('#cbGrado').val();
  var seccion = $('#cbSeccion').val();
  
  $.post( "{{ Route('mostrarAlumnos') }}", {grado: grado, seccion: seccion, _token:'{{csrf_token()}}'}).done(function(data) {
    
    $("#tabAlumno").empty();
    $("#tabAlumno").html(data.view);


  });
}


</script>

<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<link href="{{asset('../vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection