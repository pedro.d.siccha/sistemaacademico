@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')
<div class="clearfix"></div>
<div id="notificacion"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                        <div class="x_title">
                          <h2>Alumno <small>Datos Personales</small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                              </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          <br />
                          <form class="form-horizontal form-label-left input_mask">
      
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="text" class="form-control has-feedback-left" id="aluNombre" placeholder="Nombres">
                              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
      
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="text" class="form-control" id="aluApellido" placeholder="Apellidos">
                              <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                            </div>
      
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="date" class="form-control has-feedback-left" id="aluFecNacimiento" placeholder="Fecha de Nacimiento" onchange="calcularEdadAlumnoInput()">
                              <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                            </div>
      
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="text" class="form-control" id="aluEdad" placeholder="Edad" readonly>
                              <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="aluDni" placeholder="DNI" onkeyup="verificarDNI()" maxlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
    
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control" id="aluDireccion" placeholder="Dirección">
                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="aluTelefono" placeholder="Teléfono">
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
    
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control" id="aluCorreo" placeholder="Correo">
                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                            </div>
      
                            <div class="ln_solid"></div>
                            <div class="form-group">
                            </div>
      
                          </form>
                        </div>
                      </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">

      <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
          <li role="presentation" class=""><a href="#tab_Padre" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Padre</a></li>
          <li role="presentation" class=""><a href="#tab_Madre" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Madre</a></li>
          <li role="presentation" class="active"><a href="#tab_Inicio" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Apoderado</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
          <div role="tabpanel" class="tab-pane fade active in" id="tab_Inicio" aria-labelledby="home-tab">
            <p>Raw denim you probably havent heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher
              synth. Cosby sweater eu banh mi, qui irure terr.</p>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tab_Padre" aria-labelledby="profile-tab">
            <div class="x_panel">
              <div class="x_title">
                <h2><small>Registrar Datos del Padre</small></h2>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left input_mask">

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control has-feedback-left" id="padreNom" placeholder="Nombres">
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control" id="padreApe" placeholder="Apellidos">
                    <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="date" class="form-control has-feedback-left" id="padreFecNac" placeholder="Fecha de Nacimiento" onchange="calcularEdadPadre()">
                    <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control" id="padreEdad" placeholder="Edad" readonly>
                    <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="padreDni" placeholder="DNI"  onkeyup="verificarDNIApoderado()" maxlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control" id="padreDir" placeholder="Dirección">
                      <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="padreTel" placeholder="Teléfono">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control" id="padreCorreo" placeholder="Correo">
                      <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                  </div>
                  <div class="clearfix"></div>

                  <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                      <button type="button" class="btn btn-primary">Cancelar</button>
                       <button class="btn btn-primary" type="reset">Limpiar</button>
                      <button type="button" class="btn btn-success" onclick="guardar()">Guardar</button>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tab_Madre" aria-labelledby="profile-tab">
            <div class="x_panel">
              <div class="x_title">
                <h2><small>Registrar Datos de la Madre</small></h2>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left input_mask">

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control has-feedback-left" id="madreNom" placeholder="Nombres">
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control" id="madreApe" placeholder="Apellidos">
                    <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="date" class="form-control has-feedback-left" id="madreFecNac" placeholder="Fecha de Nacimiento" onchange="calcularEdadMadre()">
                    <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control" id="madreEdad" placeholder="Edad" readonly>
                    <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="madreDni" placeholder="DNI"  onkeyup="verificarDNIApoderado()" maxlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control" id="madreDir" placeholder="Dirección">
                      <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="madreTel" placeholder="Teléfono">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control" id="madreCorreo" placeholder="Correo">
                      <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                  </div>

                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                      <button type="button" class="btn btn-primary">Cancelar</button>
                       <button class="btn btn-primary" type="reset">Limpiar</button>
                      <button type="button" class="btn btn-success" onclick="guardar()">Guardar</button>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
                
        </div>
    </div>

@endsection
@section('script')
<script>

  function calcularEdadAlumnoInput() {
    var birthday = $("#aluFecNacimiento").val();
    var fechaNace = new Date(birthday);
    var fechaActual = new Date()
    var mes = fechaActual.getMonth();
    var dia = fechaActual.getDate();
    var año = fechaActual.getFullYear();
    fechaActual.setDate(dia);
    fechaActual.setMonth(mes);
    fechaActual.setFullYear(año);
    edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
    if(edad < 4){
      swal("Algo Salio Mal!", "Verifique la fecha de nacimieto", "error");
      $("#aluEdad").val("");
      $("#aluFecNacimiento").val("");
    }else{
      $("#aluEdad").val(edad);
    }
    
}

function calcularEdadApoderadoInput(){
    var birthday = $("#acoFecNacimiento").val();
    var fechaNace = new Date(birthday);
    var fechaActual = new Date()
    var mes = fechaActual.getMonth();
    var dia = fechaActual.getDate();
    var año = fechaActual.getFullYear();
    fechaActual.setDate(dia);
    fechaActual.setMonth(mes);
    fechaActual.setFullYear(año);
    edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
    if(edad < 21){
      swal("Algo Salio Mal!", "Verifique la fecha de nacimieto", "error");
      $("#aluEdad").val("");
      $("#acoFecNacimiento").val("");
    }else{
      $("#acoEdad").val(edad);
    }
    
}

function guardar(){ 
  
  var aluNombre = $('#aluNombre').val();
  var aluApellido = $('#aluApellido').val();
  var aluFecNacimiento = $('#aluFecNacimiento').val();
  var aluEdad = $('#aluEdad').val();
  var aluDni = $('#aluDni').val();
  var aluDireccion = $('#aluDireccion').val();
  var aluTelefono = $('#aluTelefono').val();
  var aluCorreo = $('#aluCorreo').val();

  var acoNombre = $('#acoNombre').val();
  var acoApellido = $('#acoApellido').val();
  var acoFecNacimiento = $('#acoFecNacimiento').val();
  var acoEdad = $('#acoEdad').val();
  var acoDni = $('#acoDni').val();
  var acoDireccion = $('#acoDireccion').val();
  var acoTelefono = $('#acoTelefono').val();
  var acoCorreo = $('#acoCorreo').val();

  var parentezco_id = $('#parentezco_id').val();
  
  $.post( "{{ Route('guardarEstudiante') }}", {aluNombre: aluNombre, aluApellido: aluApellido, aluFecNacimiento: aluFecNacimiento, aluEdad: aluEdad, aluDni: aluDni, aluDireccion: aluDireccion, aluTelefono: aluTelefono, aluCorreo: aluCorreo, acoNombre: acoNombre, acoApellido: acoApellido, acoFecNacimiento: acoFecNacimiento, acoEdad: acoEdad, acoDni:acoDni, acoDireccion: acoDireccion, acoTelefono: acoTelefono, acoCorreo: acoCorreo, parentezco_id: parentezco_id, _token:'{{csrf_token()}}'}).done(function(data) {

    if(data.resp == 1){

      swal("Buen Trabajo!", "Alumno registrado exitosamente!", "success");

        $('#aluNombre').val("");
        $('#aluApellido').val("");
        $('#aluFecNacimiento').val("");
        $('#aluEdad').val(""); 
        $('#aluDni').val("");
        $('#aluDireccion').val("");
        $('#aluTelefono').val("");
        $('#aluCorreo').val("");
        
        $('#acoNombre').val("");
        $('#acoApellido').val("");
        $('#acoFecNacimiento').val("");
        $('#acoEdad').val("");
        $('#acoDni').val("");
        $('#acoDireccion').val("");
        $('#acoTelefono').val("");
        $('#acoCorreo').val("");
      
    }else{
      swal("Algo Salio Mal!", "Alumno no pudo ser registrado!", "error");
    }
    });
}

function verificarDNI() {
  var dni = $("#aluDni").val();

  $.post( "{{ Route('verificarDNI') }}", {dni: dni, _token:'{{csrf_token()}}'}).done(function(data) {
      
      if (data.resp == 1) {
        swal("Algo Salio Mal!", "Dni ya registrado", "error");
        $("#aluDni").val("");
      }
  });
}

function verificarDNIApoderado() {
  var dni = $("#acoDni").val();

  $.post( "{{ Route('verificarDNI') }}", {dni: dni, _token:'{{csrf_token()}}'}).done(function(data) {
      
      if (data.resp == 1) {
        swal("Algo Salio Mal!", "Dni ya registrado", "error");
        $("#acoDni").val("");
      }
  });
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection