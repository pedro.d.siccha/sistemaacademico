<table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
    <thead>
        <tr>
        <th>
            <th><input type="checkbox" id="check-all" class="flat"></th>
        </th>
        <th>DNI</th>
        <th>Nombres</th>
        <th>Grado y Seccion Anterior</th>
        <th>Retirar</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($alumno as $a)
        <tr>
            <td>
            <th><input type="checkbox" id="check-all" class="flat"></th>
            </td>
            <td>{{ $a->dni }}</td>
            <td>{{ $a->alumno }}</td>
            <td>{{ $a->grado }} - {{ $a->seccion }}</td>
            <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="RETIRAR" onclick="retirarAlumno('{{ $a->estudiante_id }}', '{{ $a->matricula_id }}')"><i class="fa fa-close"></i></button></td>
        </tr>    
        @endforeach
    </tbody>
</table>