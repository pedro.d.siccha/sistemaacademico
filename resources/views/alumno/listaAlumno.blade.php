@extends('layouts.app')
@section('nombrePagina')
 Lista de Alumnos
@endsection
@section('contenido')

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Lista de Alumnos <small>MATRICULADOS</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content row">
        

          <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="nivel_id" class="form-control" required onchange="mostrarGrado()">
                <option value="">Seleccione..</option>
                @foreach ($nivel as $n)
                    <option value="{{ $n->id }}">{{ $n->nombre }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group col-md-4 col-sm-6 col-xs-12" id="cbGrado">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="cbGrado" class="form-control" required>
                <option value="">Seleccione..</option>
              </select>
            </div>
          </div>
          <div class="form-group col-md-4 col-sm-6 col-xs-12" id="cbSeccion">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Seccion <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="cbSeccion" class="form-control" required onchange="mostrarAlumnos()">
                <option value="">Seleccione..</option>
              </select>
            </div>
          </div>
        
      </div>
    </div>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
    <div class="x_title">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-left top_search">
            <div class="input-group">
            <input type="text" class="form-control" placeholder="Buscar..." id="inputBuscar">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </div>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" id="listAlumnos">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Grado</th>
                    <th>Gestión</th>
                    <th>Administración</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($alumno as $a)
                <tr>
                    <td>{{ $a->dni }}</td>
                    <td>{{ $a->nombre }} {{ $a->apellido }}</td>
                    <td>{{ $a->grado }}</td>
                    <td>
                        <button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Cambiar de Sección" onclick="verSecciones('{{ $a->grado_id }}')"><i class="fa fa-exchange"></i></button>
                        <button type="button" class="btn btn-dark btn-xs" data-toggle="tooltip" data-placement="top" title="Retirar" onclick="retirar('{{ $a->estudiante_id }}')"><i class="fa fa-ban"></i></button>
                    </td>
                    <td>
                        <button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                        <button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>

<div class="modal inmodal fade" id="listaSeccion" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">LISTA DE SECCIONES DISPONIBLES</h4>
            </div>
            <div class="modal-body" id="modalSeccion">
              
            </div>
  
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="cambiarSeccion()">Guardar</button>
            </div>
        </div>
    </div>
  </div>

@endsection
@section('script')
<script>

function mostrarGrado(){

  var idNivel = $('#nivel_id').val();
  
  $.post( "{{ Route('verCbGrado') }}", {idNivel: idNivel, _token:'{{csrf_token()}}'}).done(function(data) {
    
    $("#cbGrado").empty();
    $("#cbGrado").html(data.view);


  });
}

function mostrarSeccion(){

    var idGrado = $('#grado_id').val();
  
  $.post( "{{ Route('verCbSeccion') }}", {idGrado: idGrado, _token:'{{csrf_token()}}'}).done(function(data) {
    
    $("#cbSeccion").empty();
    $("#cbSeccion").html(data.view);


  });
}

function verSecciones(idGrado){
    $.post( "{{ Route('verCbSeccion') }}", {idGrado: idGrado, _token:'{{csrf_token()}}'}).done(function(data) {
    
        $("#modalSeccion").empty();
        $("#modalSeccion").html(data.view);
    
    
      });
    $('#listaSeccion').modal('show');
}

function cambiarSeccion(){
    $('#listaSeccion').modal('hide');
    swal("CORRECTO", "El alumno fué cambiado de sección correctamente", "success");
}

function retirar(idEstudiante){
    $.post( "{{ Route('banAlumno') }}", {idEstudiante: idEstudiante, _token:'{{csrf_token()}}'}).done(function(data) {
    
        $("#listAlumnos").empty();
        $("#listAlumnos").html(data.view);

        if(data.res == 0){
            swal("ERROR", "Código de error: " + data.resText, "error");
        }else{
            swal("CORRECTO", "El alumno fué retirado correctamente", "success");
        }
    
      });
}


</script>

<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<link href="{{asset('../vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection