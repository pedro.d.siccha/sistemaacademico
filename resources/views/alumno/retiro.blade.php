@extends('layouts.app')
@section('nombrePagina')
 Año Escolar   
@endsection
@section('contenido')
<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
<div class="x_title">
    <h2>Retiro de Estudiantes <small>MASIVA</small></h2>
    <ul class="nav navbar-right panel_toolbox">
    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <ul class="dropdown-menu" role="menu"> 
        <li><a href="#">Settings 1</a>
        </li>
        <li><a href="#">Settings 2</a>
        </li>
        </ul>
    </li>
    <li><a class="close-link"><i class="fa fa-close"></i></a>
    </li>
    </ul>
    <div class="clearfix"></div>
</div>
<div class="x_content" id="listAlumRetiro">
        <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
          <thead>
            <tr>
              <th>
                 <th><input type="checkbox" id="check-all" class="flat"></th>
              </th>
              <th>DNI</th>
              <th>Nombres</th>
              <th>Grado y Seccion Anterior</th>
              <th>Retirar</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($alumno as $a)
              <tr>
                <td>
                  <th><input type="checkbox" id="check-all" class="flat"></th>
                </td>
                <td>{{ $a->dni }}</td>
                <td>{{ $a->nombre }} {{ $a->apellido }}</td>
                <td>1 - A</td>
                <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="RETIRAR" onclick="retirarAlumno('{{ $a->id }}')"><i class="fa fa-close"></i></button></td>
              </tr>    
            @endforeach
          </tbody>
        </table>
      </div>
</div>
</div>

@endsection
@section('script')
<script>
function retirarAlumno(estudiante_id){ 

  $.post( "{{ Route('retirarAlumno') }}", {estudiante_id: estudiante_id, _token:'{{csrf_token()}}'}).done(function(data) {
                $("#listAlumRetiro").empty();
                $("#listAlumRetiro").html(data.notificacion);

            });
  
}


</script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<link href="{{asset('../vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection