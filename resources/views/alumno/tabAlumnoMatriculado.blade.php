<table class="table table-hover">
    <thead>
        <tr>
            <th>DNI</th>
            <th>Nombre</th>
            <th>Grado</th>
            <th>Gestión</th>
            <th>Administración</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($alumno as $a)
        <tr>
            <td>{{ $a->dni }}</td>
            <td>{{ $a->nombre }} {{ $a->apellido }}</td>
            <td>{{ $a->grado }}</td>
            <td>
                <button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Cambiar de Sección" onclick="verSecciones('{{ $a->grado_id }}')"><i class="fa fa-exchange"></i></button>
                <button type="button" class="btn btn-dark btn-xs" data-toggle="tooltip" data-placement="top" title="Retirar" onclick="retirar('{{ $a->estudiante_id }}')"><i class="fa fa-ban"></i></button>
            </td>
            <td>
                <button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                <button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>