<table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
    <thead>
      <tr>
        <th>
           <th><input type="checkbox" id="check-all" class="flat"></th>
        </th>
        <th>DNI</th>
        <th>Nombres</th>
        <th>Grado y Seccion Anterior</th>
      </tr>
    </thead>


    <tbody>
      @foreach ($alumnos as $a)
      <tr>
        <td>
           <th><input type="checkbox" id="check-all" class="flat"></th>
        </td>
        <td>{{ $a->dni }}</td>
        <td>{{  $a->nombre }} {{ $a->apellido }}</td>
        <td>1ro - A</td>
      </tr>    
      @endforeach 
    </tbody>
  </table>