@extends('layouts.app')
@section('nombrePagina')
 Reuniones de APAFA   
@endsection
@section('contenido')
<div class="clearfix"></div>
<div class="container body">
    <div class="main_container">

      <!-- page content -->
      <div class="" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Contactos</h3>
            </div>

            <div class="title_right">
              <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Buscar contacto...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Buscar!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="clearfix"></div>

          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Contactos <small>conectados</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Administrativos</a>
                      </li>
                      <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Docentes</a>
                      </li>
                      <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Alumnos</a>
                      </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-left top_search">
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Buscar Encargado...">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Buscar!</button>
                              </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_title">
                                <h2>Resultados de busqueda <small>Seleccione un encargado</small></h2>
                                <div class="clearfix"></div>
                              </div>
                              <div class="x_content">
            
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Nombres</th>
                                      <th>Apellidos</th>
                                      <th>Seleccionar</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <th scope="row">1</th>
                                      <td>Juan Gomez</td>
                                      <td>45879562</td>
                                      <td>@mdo</td>
                                    </tr>
                                  </tbody>
                                </table>
            
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_title">
                                <h2>Asistentes <small>Elegir asistentes a la reunión</small></h2>
                                <div class="clearfix"></div>
                              </div>
            
                              <div class="x_content">
            
                                <div class="table-responsive">
                                  <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                      <tr class="headings">
                                        <th>
                                          <input type="checkbox" id="check-all" class="flat">
                                        </th>
                                        <th class="column-title">Codigo </th>
                                        <th class="column-title">Nombres </th>
                                        <th class="column-title">DNI </th>
                                        <th class="column-title">Área </th>
                                        </th>
                                        <th class="bulk-actions" colspan="7">
                                          <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                        </th>
                                      </tr>
                                    </thead>
            
                                    <tbody>
                                      <tr class="even pointer">
                                        <td class="a-center ">
                                          <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td class=" ">121000040</td>
                                        <td class=" ">Pablo Ramirez </td>
                                        <td class=" ">121000210 </td>
                                        <td class=" ">Secretaría</td>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                        
                              </div>
                            </div>
                          </div>

                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                        <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-left top_search">
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Buscar Encargado...">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Buscar!</button>
                              </span>
                            </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                        <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-left top_search">
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Buscar Encargado...">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Buscar!</button>
                              </span>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>

          <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Mensaje 001</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <div class="bs-example" data-example-id="simple-jumbotron">
                  <div class="jumbotron">
                    <h1>Buenas tardas!</h1>
                    <p>Este es el primer mensaje.</p>
                  </div>
                </div>

              </div>
            </div>
          </div>
          
        </div>
        <div class="clearfix"></div>
      </div>
      <!-- /page content -->
    </div>
  </div>
@endsection