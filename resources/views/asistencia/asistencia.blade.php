@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')


<div class="clearfix"></div> 
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filtro <small>Registro de Asistencia</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content row">

        @foreach ($curso as $c)
        
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="listaAlumnos('{{ $c->grado_id }}')">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-file-text-o"></i>
            </div>
            <div class="count">15</div>

            <h3>{{ $c->curso }}</h3>
            <p>Grado: {{ $c->grado }}</p>
          </div>
        </div>

        @endforeach

        <div class="ln_solid"></div>

        <div class="row" id="tabAsistencia">
        </div> 


      </div>
    </div>
  </div>
</div>


<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12 row" id="divListaAlumno">

</div>


@endsection
@section('script')
<script>
function buscarAlumnoAsistencia(){
  
  var idGrado = $('#grado_id').val();
  
  $.post( "{{ Route('buscarAlumnoAsistencia') }}", {idGrado: idGrado, _token:'{{csrf_token()}}'}).done(function(data) {

      $("#divListaAlumno").empty();
      $("#divListaAlumno").html(data.tabAlumnos);

    });
}

function listaAlumnos(idCursoGrado){ 
  
  $.post( "{{ Route('buscarAlumnoAsistencia') }}", {idGrado: idCursoGrado, _token:'{{csrf_token()}}'}).done(function(data) {

      $("#divListaAlumno").empty();
      $("#divListaAlumno").html(data.tabAlumnos);

    });

  
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection