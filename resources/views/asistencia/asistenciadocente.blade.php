@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filtro <small>Registro de Asistencia</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel Académico <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="nivel_id" class="form-control" required onchange="verDocentes()">
                <option value="">Seleccione..</option>
                @foreach ($nivel as $n)
                  <option value="{{ $n->id }}">{{ $n->nombre }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="col-md-12 col-sm-6 col-xs-12" id="tabDocentes">

          </div>
         
          
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="button" onclick="buscarDocente()">Buscar</button>
              <button type="submit" class="btn btn-success">Limpiar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>


<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12" id="divDocenteAsistencia">

</div>

@endsection
@section('script')
<script>
function buscarDocente(){ 
  $.post( "{{ Route('buscarDocenteAsistencia') }}", {_token:'{{csrf_token()}}'}).done(function(data) {

    $("#divDocenteAsistencia").empty();
    $("#divDocenteAsistencia").html(data.tabDocente);

  });
}

function verDocentes(){ 

  var idNivel = $('#nivel_id').val();

  $.post( "{{ Route('verDocenteAsistencia') }}", {idNivel: idNivel, _token:'{{csrf_token()}}'}).done(function(data) {

    $("#tabDocentes").empty();
    $("#tabDocentes").html(data.tabDocente);

  });
  
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection