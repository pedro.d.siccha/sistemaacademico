<div class="col-md-8 col-sm-8 col-xs-12">
    <div class="x_title">
        <h2>Registrar tema y asistencia de los alumnos <small>{{ date('d-m-Y') }}</small></h2>
        <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_title">
      <div class="row">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">TEMA: </label>
        <div class="col-md-7 col-sm-8 col-xs-12">
          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Ingresar Tema">
        </div>
        <button class="btn btn-primary" type="button" onclick="guardarTema()">Guardar</button>
      </div>
    </div>
    <div class="x_content" id="tabAsistencia">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th>
                     <th><input type="checkbox" id="check-all" class="flat"></th>
                  </th>
                  <th>DNI</th>
                  <th>Nombres</th>
                  <th>Estado</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($estudiantes as $e)    
                <tr>
                  <td>
                     <th><input type="checkbox" id="check-all" class="flat" ></th>
                  </td>
                  <td>{{ $e->dni }}</td>
                  <td>{{ $e->nombre }} {{ $e->apellido }}</td>
                  <td><span class="badge bg-orange">PENDIENTE</span></td>
                  <td>
                    <button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Marcar Tardanza">T</button>
                    <button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Marcar Falta">F</button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
    
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn btn-primary" type="button" onclick="enviarAsistencia()">Enviar</button>
                  <button type="submit" class="btn btn-success">Limpiar</button>
                </div>
              </div>
    
          </div>
          
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="x_title">
          <h2>Verificar</h2>
          <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
      </div>
      <div class="x_content" id="tabCantAsistencia">
              <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                <thead>
                  <tr>
                    <th>Condición</th>
                    <th>Cantidad</th>
                    <th>Porcentaje</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            
      </div>
    </div>

    <script>
        function enviarAsistencia(){ 
          
          $.post( "{{ Route('enviarAsistencia') }}", {_token:'{{csrf_token()}}'}).done(function(data) {
        
            
            $("#tabCantAsistencia").empty();
            $("#tabCantAsistencia").html(data.notificacion);
            $("#tabAsistencia").empty();
            $("#tabAsistencia").html(data.asistencia);
            swal("CORRECTO", "Asisitecia registrada correctamente", "success");
        
            });
        }
    </script>