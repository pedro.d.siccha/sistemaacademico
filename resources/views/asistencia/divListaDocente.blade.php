<div class="x_panel">
    <div class="x_title">
        <h2>Asistencia <small>MASIVA</small></h2>
        <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
            </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th>
                     <th><input type="checkbox" id="check-all" class="flat"></th>
                  </th>
                  <th>DNI</th>
                  <th>Nombres</th>
                  <th>Grado y Seccion</th>
                </tr>
              </thead>
    
    
              <tbody>
                @foreach ($docente as $d)
                  <tr>
                    <td>
                       <th><input type="checkbox" id="check-all" class="flat"></th>
                    </td>
                    <td>{{ $d->dni }}</td>
                    <td>{{ $d->nombre }} {{ $d->apellido }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
    
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn btn-primary" type="button" onclick="enviarAsistencia()">Enviar</button>
                  <button type="submit" class="btn btn-success">Limpiar</button>
                </div>
              </div>
    
          </div>
          
    </div>

    <script>
        function enviarAsistencia(){ 
          $.post( "{{ Route('enviarAsistencia') }}", {_token:'{{csrf_token()}}'}).done(function(data) {
              swal("CORRECTO", "La asistencia fué enviada correctamente.", "success");
              $("#divDocenteAsistencia").empty();
              $("#divDocenteAsistencia").html(data.notificacion);
        
            });
        }
    </script>