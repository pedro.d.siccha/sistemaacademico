<table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
    <thead>
      <tr>
        <th>
           <th><input type="checkbox" id="check-all" class="flat"></th>
        </th>
        <th>DNI</th>
        <th>Nombres</th>
        <th>Estado</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($estudiantes as $e)    
      <tr>
        <td>
           <th><input type="checkbox" id="check-all" class="flat" ></th>
        </td>
        <td>{{ $e->dni }}</td>
        <td>{{ $e->nombre }} {{ $e->apellido }}</td>
        <td><span class="badge bg-info">ASISTIO</span></td>
        <td>
          <button type="button" class="btn btn-dark btn-xs" data-toggle="tooltip" data-placement="top" title="Marcar Tardanza">E</button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>