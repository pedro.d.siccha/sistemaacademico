<table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
    <thead>
      <tr>
        <th>
           <th><input type="checkbox" id="check-all" class="flat"></th>
        </th>
        <th>Foto</th>
        <th>DNI</th>
        <th>Nombres</th>
        <th>Curso</th>
        <th>Grado</th>
        <th>Estado</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($docentes as $d)    
      <tr>
        <td>
           <th><input type="checkbox" id="check-all" class="flat" ></th>
        </td>
        <td>{{ $d->foto }}</td>
        <td>{{ $d->dni }}</td>
        <td>{{ $d->nombre }} {{ $d->apellido }}</td>
        <td>{{ $d->curso }}</td>
        <td>{{ $d->nivel }} {{ $d->grado }}</td>
        <td><span class="badge bg-orange">PENDIENTE</span></td>
        <td>
          <button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Marcar Tardanza">T</button>
          <button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Marcar Falta">F</button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>