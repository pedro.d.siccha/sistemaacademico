@extends('layouts.login')

@section('contenido')
<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
  <div class="card card0 border-0">
      <div class="row d-flex">
          <div class="col-lg-6">
              <div class="card1 pb-5">
                  <div class="row"> <img src="{{ ('img/sistema/Logo.png') }}" class="logo"> </div>
                  <div class="row px-3 justify-content-center mt-4 mb-5 border-line"> 
                      <img src="{{ ('img/sistema/secundario.jpg') }}" class="image"> 
                  </div>
              </div>
          </div>
          <div class="col-lg-6">
              <form method="POST" action="{{ route('login') }}">
                  @csrf
              <div class="card2 card border-0 px-4 py-5">
                  <div class="row px-3 mb-4">
                      <h1 class="font-weight-bold">COLEGIO</h1>
                  </div>
                  <div class="row px-3"> <label class="mb-1">
                          <h6 class="mb-0 text-sm">Usuario</h6>
                      </label> 
                      <input id="email" type="email" class="mb-4 form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo" autofocus>

                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  <div class="row px-3"> <label class="mb-1">
                          <h6 class="mb-0 text-sm">Contraseña</h6>
                      </label> 
                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña">

                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                  <div class="row mb-3 px-3"> 
                      <button type="submit" class="btn btn-blue text-center">Ingresar</button> 
                  </div>
              </div>
              </form>

          </div>
      </div>
      <div class="bg-blue py-4">
          <div class="row px-3"> 
              <small class="ml-4 ml-sm-5 mb-2">InfoRad.</small>
          </div>
      </div>
  </div>
</div>
@endsection