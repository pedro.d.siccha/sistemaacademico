@extends('layouts.app')
@section('nombrePagina')
 Lista de Cursos
@endsection
@section('contenido')

<div class="clearfix"></div> 
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Lista de Cursos</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content row">

        @foreach ($cursos as $c)
        
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="administrarAsistencia()">
          <div class="tile-stats">
            <div class="icon">
                <i class="fa fa-check-square-o"></i>
            </div>
            <div class="count">Curso:</div>
            <h3>{{ $c->nombre }}</h3>
          </div>
        </div>

        @endforeach

        <div class="ln_solid"></div>

        <div class="row" id="divAsistencia"></div> 

      </div>
    </div>
  </div>
</div>


<div class="clearfix"></div>

@endsection
@section('script')
<script>
function administrarAsistencia(){
  
  $.post( "{{ Route('panelAsistencia') }}", {_token:'{{csrf_token()}}'}).done(function(data) {

      $("#divAsistencia").empty();
      $("#divAsistencia").html(data.panelAsistencia);

    });
}

</script>
@endsection