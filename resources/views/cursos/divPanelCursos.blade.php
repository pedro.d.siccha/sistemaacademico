<div class="clearfix"></div> 
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Panel de Control de Cursos</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content row">

        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab_competencias" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Competencias</a>
            </li>
            <li role="presentation" class=""><a href="#tab_items" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Items</a>
            </li>
            <li role="presentation" class=""><a href="#tab_notas" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Notas</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_competencias" aria-labelledby="home-tab">

              <div class="form-group">
                <label class="col-sm-3 control-label">Elegir un Periodo</label>

                <div class="col-sm-9">
                  
                  <select class="form-control">
                    <option>Seleccionar Perido</option>
                    <option>1er Trimestre</option>
                    <option>2do Trimestre</option>
                    <option>3er Trimestre</option>
                  </select>

                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Ingresar Competencia</label>

                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Ingresar competencias">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary">Guardar</button>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Competencias</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Competencia</th>
                          <th>Trimestre</th>
                          <th>Administración</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row"></th>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_items" aria-labelledby="profile-tab">

              <div class="form-group">
                <label class="col-sm-3 control-label">Elegir una Competencia</label>

                <div class="col-sm-9">
                  
                  <select class="form-control">
                    <option>Seleccionar Perido</option>
                  </select>

                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Ingresar Tema</label>

                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Ingresar tema">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary">Guardar</button>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Temas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Temas</th>
                          <th>Competencias</th>
                          <th>Trimestre</th>
                          <th>Administración</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row"></th>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
              
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_notas" aria-labelledby="profile-tab">

              <div class="form-group">
                <label class="col-sm-3 control-label">Elegir un Tema</label>

                <div class="col-sm-9">
                  
                  <select class="form-control">
                    <option>Seleccionar Perido</option>
                  </select>

                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Ingresar Calificación</label>

                <div class="col-sm-9">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Ingresar nombre de la calificación">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary">Guardar</button>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Notas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nota</th>
                          <th>Tema</th>
                          <th>Competencia</th>
                          <th>Trimestre</th>
                          <th>Administración</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row"></th>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="clearfix"></div>