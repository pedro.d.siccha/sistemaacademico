<div class="clearfix"></div> 
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Panel de Control de Exámenes</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content row">

        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab_programar" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Programar Exámenes</a>
            </li>
            <li role="presentation" class=""><a href="#tab_examenes" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Ver Exámenes</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_programar" aria-labelledby="home-tab">

              <div class="form-group">
                <label class="col-sm-3 control-label">Elegir un Nombre de Nota</label>

                <div class="col-sm-9">
                  
                  <select class="form-control">
                    <option>Seleccionar Nota</option>
                  </select>

                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Fecha y Hora</label>

                <div class='input-group date' id='myDatepicker'>
                  <input type='text' class="form-control" />
                  <span class="input-group-addon">
                     <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Competencias</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Competencia</th>
                          <th>Trimestre</th>
                          <th>Administración</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row"></th>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_examenes" aria-labelledby="profile-tab">

              <div class="form-group">
                <div id='calendar'></div>
              </div>
              
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="clearfix"></div>
<script src="{{ ('../vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ ('../vendors/fullcalendar/dist/fullcalendar.min.js') }}"></script>
<link href="{{ ('../vendors/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet">
<link href="{{ ('../vendors/fullcalendar/dist/fullcalendar.print.css') }}" rel="stylesheet" media="print">
