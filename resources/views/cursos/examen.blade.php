@extends('layouts.app')
@section('nombrePagina')
 Lista de Cursos
@endsection
@section('contenido')

<div class="clearfix"></div> 
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Lista de Cursos</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content row">

        @foreach ($cursos as $c)
        
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="administrarExamen()">
          <div class="tile-stats">
            <div class="icon">
                <i class="fa fa-file-text-o"></i>
            </div>
            <div class="count">Curso:</div>
            <h3>{{ $c->nombre }}</h3>
          </div>
        </div>

        @endforeach

        <div class="ln_solid"></div>

        <div class="row" id="divExamen"></div> 

      </div>
    </div>
  </div>
</div>


<div class="clearfix"></div>

@endsection
@section('script')

  <!-- bootstrap-daterangepicker -->
  <link href="{{ asset('../vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <!-- bootstrap-datetimepicker -->
  <link href="{{ asset('../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

  <!-- Bootstrap Colorpicker -->
  <link href="{{ asset('../vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
  <!-- FastClick -->
  <script src="{{ asset('../vendors/fastclick/lib/fastclick.js') }}"></script>
  <!-- bootstrap-daterangepicker -->
  <script src="{{ asset('../vendors/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('../vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- bootstrap-datetimepicker -->    
  <script src="{{ asset('../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
  <!-- Bootstrap Colorpicker -->
  <script src="{{ asset('../vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
  
  <script>
    $('#myDatepicker').datetimepicker();
    
    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'hh:mm A'
    });
    
    $('#myDatepicker4').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });

    $('#datetimepicker6').datetimepicker();
    
    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });
    
    $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    
    $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
</script>

<script>
function administrarExamen(){

  $.post( "{{ Route('panelExamen') }}", {_token:'{{csrf_token()}}'}).done(function(data) {

      $("#divExamen").empty();
      $("#divExamen").html(data.panelExamen);

    });

}

</script>
@endsection