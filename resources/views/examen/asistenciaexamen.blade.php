@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')

<div class="" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Notas</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Buscar...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Notas</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="form-group col-xs-12">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel Académico <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select id="heard" class="form-control" required>
                  <option value="">Seleccione..</option>
                  <option value="press">Inicial</option>
                  <option value="net">Primaria</option>
                  <option value="mouth">Secundaria</option>
                  </select>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-xs-6">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado Académico <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select id="heard" class="form-control" required>
                  <option value="">Seleccione..</option>
                  <option value="press">1ro</option>
                  <option value="net">2do</option>
                  <option value="mouth">3ro</option>
                  <option value="mouth">4to</option>
                  <option value="mouth">5to</option>
                  <option value="mouth">6to</option>
                  </select>
              </div>
            </div>

            <div class="form-group col-xs-6">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Sección <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select id="heard" class="form-control" required>
                  <option value="">Seleccione..</option>
                  <option value="press">A</option>
                  <option value="net">B</option>
                  <option value="mouth">C</option>
                  </select>
              </div>
            </div>

            <div class="form-group col-xs-12">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Cursos <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select id="heard" class="form-control" required>
                  <option value="">Seleccione..</option>
                  <option value="press">Matemática</option>
                  <option value="net">CTA</option>
                  <option value="mouth">Historia</option>
                  </select>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12" id="listAlumno">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Alumnos <small>Por examen</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                    <thead>
                      <tr>
                        <th>
                           <th><input type="checkbox" id="check-all" class="flat"></th>
                        </th>
                        <th>DNI</th>
                        <th>Nombres</th>
                        <th>Grado y Seccion Anterior</th>
                      </tr>
                    </thead>
          
          
                    <tbody>
                      <tr>
                        <td>
                           <th><input type="checkbox" id="check-all" class="flat"></th>
                        </td>
                        <td>11111111</td>
                        <td>Jesus Mamani Rosales</td>
                        <td>1ro - A</td>
                      </tr>
                      <tr>
                       <td>
                           <th><input type="checkbox" id="check-all" class="flat"></th>
                        </td>
                        <td>22222222</td>
                        <td>Roberto Maguiña Santos</td>
                        <td>1ro - A</td>
                      </tr>
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                 <button class="btn btn-primary" type="reset">Limpiar</button>
                <button type="button" class="btn btn-success" onclick="guardarExamen()">Guardar</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('script')
<script>
function guardarExamen(){ 
  $.post( "{{ Route('enviarAsistenciaExamen') }}", {_token:'{{csrf_token()}}'}).done(function(data) {

    $("#listAlumno").empty();
    $("#listAlumno").html(data.tabLista);

  });
}

function EditarPaciente(){ 

  var nombre = $('#nombre').val();
  var apellido = $('#apellido').val();
  var fecnac = $('#fecnac').val();
  var dni = $('#dni').val();
  var direccion = $('#direccion').val();
  var telefono = $('#telefono').val();
  var lugarnac = $('#lugarnac').val();
  var lugarproc = $('#lugarproc').val();
  var correo = $('#correo').val();
  var ocu = $('#ocu').val();
  var inst = $('#inst').val();
  var eci = $('#eci').val();
  var gnero = $('#gnero').val();
  var idPaciente = $('#idPaciente').val();

  
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection