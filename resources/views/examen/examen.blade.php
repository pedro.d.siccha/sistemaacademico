@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')
<div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                        <div class="x_title">
                          <h2>Registrar Exámenes <small></small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                              </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          <br />
                          <form class="form-horizontal form-label-left input_mask">
      
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                              <input type="date" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Fecha del Exámen">
                              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
      
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select id="heard" class="form-control" required>
                                    <option value="">Seleccionar Grado..</option>
                                    <option value="press">PRIMERO</option>
                                    <option value="net">SEGUNDO</option>
                                    <option value="mouth">TERCERO</option>
                                    <option value="mouth">CUARTO</option>
                                    <option value="mouth">QUINTO</option>
                                    <option value="mouth">SEXTO</option>
                                </select>
                            </div>
      
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select id="heard" class="form-control" required>
                                    <option value="">Seleccionar SECCION..</option>
                                    <option value="press">A</option>
                                    <option value="net">B</option>
                                </select>
                            </div>
      
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select id="heard" class="form-control" required>
                                    <option value="">Seleccionar Área..</option>
                                    <option value="press">Matemática</option>
                                </select>
                            </div>

                           
      
                            <div class="ln_solid"></div>
                            <div class="form-group">
                              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <button type="button" class="btn btn-primary">Cancelar</button>
                                 <button class="btn btn-primary" type="reset">Limpiar</button>
                                <button type="button" class="btn btn-success" onclick="guardarExamen()">Guardar</button>
                              </div>
                            </div>
      
                          </form>
                        </div>
                      </div>
        </div>
        <div id="divListaExamen"
    </div>

@endsection
@section('script')
<script>
function guardarExamen(){ 
  $.post( "{{ Route('guadarExamen') }}", {_token:'{{csrf_token()}}'}).done(function(data) {

    $("#divListaExamen").empty();
    $("#divListaExamen").html(data.tabExamen);

  });
}

function EditarPaciente(){ 

  var nombre = $('#nombre').val();
  var apellido = $('#apellido').val();
  var fecnac = $('#fecnac').val();
  var dni = $('#dni').val();
  var direccion = $('#direccion').val();
  var telefono = $('#telefono').val();
  var lugarnac = $('#lugarnac').val();
  var lugarproc = $('#lugarproc').val();
  var correo = $('#correo').val();
  var ocu = $('#ocu').val();
  var inst = $('#inst').val();
  var eci = $('#eci').val();
  var gnero = $('#gnero').val();
  var idPaciente = $('#idPaciente').val();

  
}

function EliminarPaciente(idPac){ 


}


</script>
@endsection