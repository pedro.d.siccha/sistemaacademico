<div class="x_panel">
    <div class="x_title">
      <h2>Lista de Alumnos <small>Por notas</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Nombres</th>
            <th>DNI</th>
            <th>Libreta</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>11111111</td>
            <td><button class="btn btn-primary" type="button"><i class="fa fa-download"></i> DESCARGAR</button></td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>22222222</td>
            <td><button class="btn btn-primary" type="button"><i class="fa fa-download"></i> DESCARGAR</button></td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>333333333</td>
            <td><button class="btn btn-primary" type="button"><i class="fa fa-download"></i> DESCARGAR</button></td>
          </tr>
        </tbody>
      </table>

    </div>
  </div>