@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')

<div class="" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Docentes</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Buscar...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Lista de Docentes</h2><button type="button" class="btn btn-default btn-sm" onclick="modalDocente()"><i class="fa fa-plus"></i></button>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="form-group col-xs-12">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel Académico <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select id="heard" class="form-control" required>
                  <option value="">Seleccione..</option>
                  <option value="press">Inicial</option>
                  <option value="net">Primaria</option>
                  <option value="mouth">Secundaria</option>
                  </select>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Alumnos <small>Matriculados</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" id="tabDocente">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Foto</th>
                        <th>Nombre</th>
                        <th>DNI</th>
                        <th>Telefono</th>
                        <th>Dirección</th>
                        <th>Usuario</th>
                        <th>Título</th>
                        <th>Gestión</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($docente as $d)
                      <tr>
                        <th scope="row">{{ $d->docente_id }}</th>
                        <td>{{ $d->nombre }} {{ $d->apellido }}</td>
                        <td>{{ $d->dni }}</td>
                        <td>{{ $d->telefono }}</td>
                        <td>{{ $d->direccion }}</td>
                        <td>{{ $d->usuario }}</td>
                        <td>{{ $d->titulo }}</td>
                        <td>@mdo</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Agregar Docente -->

<div class="modal fade" id="agregarDocente" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <form id="fdocente" name="fdocente" method="post" action="guardarDocente" class="formDocente" enctype="multipart/form-data">
      <input type="text" name="_token" id="_token" hidden value="{{ csrf_token() }}">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Asignar Docentes </h4>
          </div>
          <div class="modal-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Ingresar Nombres" id="nombre" name="nombre">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
              </span>
            </div>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Ingresar Apellidos" id="apellidos" name="apellidos">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
              </span>
            </div>
            <div class="input-group">
              <input type="number" class="form-control" placeholder="Ingresar DNI" id="dni" name="dni">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
              </span>
            </div>
            <div class="input-group">
              <input type="email" class="form-control" placeholder="Ingresar Correo" id="correo" name="correo">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
              </span>
            </div>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Ingresar Teléfono" id="telefono" name="telefono">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
              </span>
            </div>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Ingresar Dirección" id="direccion" name="direccion">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
              </span>
            </div>
            <div class="input-group">
              <label class="col-sm-2 col-form-label">Género</label>
                <div class="i-checks">
                  <label> 
                      <input type="radio" value="Masculino" name="a" id="genMasculino"> 
                      <i></i> Masculino 
                  </label>
                </div>
                <div class="i-checks">
                    <label> 
                        <input type="radio" checked="" value="Femenino" name="a" id="genFemenino"> 
                        <i></i> Femenino 
                    </label>
                </div>
            </div>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Ingresar Titulo" id="titulo" name="titulo">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-user"></i></button>
              </span>
            </div>
            <div class="input-group">
              <label class="col-sm-2 col-form-label">Foto</label>
              <div class="custom-file">
                  <input id="logo" type="file" class="custom-file-input" id="foto" name="foto">
                  <label for="logo" class="custom-file-label">Seleccionar...</label>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
          </div>
      </div>
    </form>
  </div>
</div>
<!-- Fin Docentes -->

@endsection
@section('script')

<script>

function modalDocente(){ 
  $('#agregarDocente').modal('show');
}

$(document).on("submit",".formDocente",function(e){
        
  e.preventDefault();
  var formu = $(this);
  var nombreform = $(this).attr("id");
  
  if ($('#genMasculino').is(":checked"))
  {
      var genero = "Masculino";
  }else{
      var genero = "Femenino";
  }
  
  if (nombreform == "fdocente") {
      var miurl = "{{ Route('guardarDocente') }}";
  }
  var formData = new FormData($("#"+nombreform+"")[0]);
  
  $.ajax({ url: miurl, type: 'POST', data: formData, cache: false, contentType: false, processData: false,
          beforeSend: function(){
              setTimeout(function() {
                  toastr.options = {
                      closeButton: true,
                      progressBar: true,
                      showMethod: 'slideDown',
                      timeOut: 4000,
                      positionClass: 'toast-top-center'
                  };
                  toastr.info('Subiendo Archivos, por favor espere');

              }, 1300);
          },
          success: function(data){
              $("#tabDocente").empty();
              $("#tabDocente").html(data.view);
              $('#agregarDocente').modal('hide');
              swal("CORRECTO", "El docente fué registrado correctamente", "success");
          },
          error: function(data) {
              swal("ERROR", "Hubo un problema al registrar al docente", "error");
          }
  });
});

function EliminarPaciente(idPac){ 


}


</script>
@endsection