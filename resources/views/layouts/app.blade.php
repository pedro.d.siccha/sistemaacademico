<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>SISTEMA GESTIÓN ACADÉMICA! | @yield('nombrePagina') </title>

    <!-- Bootstrap -->
    <link href="{{asset('../vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('../vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('../vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('../vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="{{asset('../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{asset('../vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('../vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('../build/css/custom.min.css')}}" rel="stylesheet">
    <link href="{{asset('../build/css/sweetalert/sweetalert.css')}}" rel="stylesheet">

    <link href="{{asset('../vendors/toastr/toastr.min.css')}}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{ route('home') }}" class="site_title"><i class="fa fa-paw"></i> <span>SISTEMA GESTIÓN ACADÉMICA!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{asset('production/images/img.jpg')}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido,</span>
                <h2>Administrador</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menú Principal</h3>
                <ul class="nav side-menu">
                  <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> Inicio</a></li>
                  <li><a><i class="fa fa-folder-o"></i> Matrícula <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('matricula') }}">Matrícula</a></li>
                      <li><a href="{{ route('matriculaMasiva') }}">Matrícula Masiva</a></li>
                      <li><a href="{{ route('traslados') }}">Gestión de Traslados</a></li>
                      <li><a href="{{ route('nominas') }}">Nóminas</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-graduation-cap"></i> Académico <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('anioEscolar') }}">Año Escolar</a></li>
                      <li><a href="{{ route('area') }}">Cursos</a></li>
                      <li><a href="{{ route('planEstudios') }}">Plan de Estudios</a></li>
                      <li><a href="{{ route('nivel') }}">Nivel Educativo</a></li>
                      <li><a href="{{ route('grado') }}">Grado</a></li>
                      <li><a href="{{ route('seccion') }}">Sección</a></li>
                      <li><a href="{{ route('asignacion') }}">Asignación</a></li>
                      <li><a href="{{ route('horarios') }}">Horarios</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-child"></i> Alumnos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('crearEstudiante') }}">Registro de Estudiantes</a></li>
                      <li><a href="{{ route('listaAlumnos') }}">Lista de Alumnos</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Asistencia <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('asistencia') }}">Asistencia</a></li>
                      <li><a href="{{ route('asistenciaDocente') }}">Asistencia de Docentes</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-file-o"></i>Administración Cursos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('administrarCursos') }}">Cursos</a></li>
                      <li><a href="{{ route('administrarExamen') }}">Examenes</a></li>
                      <li><a href="{{ route('administraNotas') }}">Notas</a></li>
                      <li><a href="{{ route('administrarAsistencia') }}">Asistencia</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-star"></i>Calificación <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('calificacion') }}">Calificación</a></li>
                      <li><a href="{{ route('porcentajeCalificacion') }}">Porcentaje de Calificación</a></li>
                      <li><a href="{{ route('acta') }}">Acta</a></li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-envelope-o"></i>Mensajeria <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('contactos') }}">Contactos</a></li>
                     <!-- 
                      <li><a href="{{ route('smsEnviados') }}">Mensajes Enviados</a></li>
                      <li><a href="{{ route('smsRecibidos') }}">Mensajes Recibidos</a></li>
                     -->
                    </ul>
                  </li>

                  <li><a><i class="fa fa-group"></i>APAFA <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('reuniones') }}">Reuniones</a></li>
                      <li><a href="{{ route('caja') }}">Caja</a></li>
                      <li><a href="{{ route('inventario') }}">Inventario</a></li>
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-line-chart"></i>Finanzas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('flujoCaja') }}">Pago</a></li>
                      <li><a href="{{ route('flujoCaja') }}">Flujo de Caja</a></li>
                      <li><a href="{{ route('pagos') }}">Gestion de Pagos</a></li>
                    </ul>
                  </li>
                  <!--
                  <li><a><i class="fa fa-warning"></i>Notificaciones <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('noticias') }}">Noticias</a></li>
                      <li><a href="{{ route('eventos') }}">Eventos</a></li>
                      <li><a href="{{ route('vacaciones') }}">Vacaciones</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-pie-chart"></i>Reportes <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('reporteClases') }}">Reporte de Clases</a></li>
                      <li><a href="{{ route('reporteAsistencia') }}">Reporte de Asistencia</a></li>
                      <li><a href="{{ route('reporteEstudiante') }}">Reporte del Estudiante</a></li>
                    </ul>
                  </li>
                -->
                  <li><a><i class="fa fa-user"></i>Administrador <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('plantillaCorreo') }}">Plantilla de Correo</a></li>
                      <li><a href="{{ route('seguridad') }}">Seguridad</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cogs"></i>Ajustes Generales <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('controlPersonal') }}">Control de Personal</a></li>
                      <li><a href="{{ route('ajustesBasicos') }}">Ajustes Básicos</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-users"></i>Gestión de Usuarios <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('padres') }}">Padres</a></li>
                      <li><a href="{{ route('docentes') }}">Docentes</a></li>
                      <li><a href="{{ route('alumnos') }}">Alumnos</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="{{asset('production/images/img.jpg')}}" alt="">Administrador
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Perfil</a></li>
                    <li>
                      <a href="javascript:;">
                        <span>Configuración</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Ayuda</a></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="produccion/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
         @yield('contenido')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('../vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('../vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('../vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('../vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{asset('../vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{asset('../vendors/gauge.js/dist/gauge.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{asset('../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('../vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{asset('../vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{asset('../vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{asset('../vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('../vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('../vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('../vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{asset('../vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{asset('../vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{asset('../vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{asset('../vendors/DateJS/build/date.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('../vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
    <script src="{{asset('../vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('../vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('../vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

    <script src="{{ asset('../vendors/pnotify/dist/pnotify.js') }}"></script>
    <script src="{{ asset('../vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
    <script src="{{ asset('../vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
    <script src="{{asset('../vendors/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('../vendors/toastr/toastr.min.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{asset('../build/js/custom.min.js')}}"></script>
    @yield('script')
  </body>
</html>
