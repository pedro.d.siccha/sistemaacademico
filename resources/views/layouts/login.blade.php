<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>SISTEMA GESTIÓN ACADÉMICA! | @yield('nombrePagina') </title>

    <link href="{{asset('/css/login.css')}}" rel="stylesheet">
  </head>

  <body class="nav-md">
        <div class="right_col" role="main">
         @yield('contenido')
        </div>
        <!-- /page content -->
        <!-- /footer content -->
      </div>
    </div>
  </body>
</html>
