<label class="control-label col-md-3 col-sm-3 col-xs-12">Grado Académico <span class="required">*</span>
</label>
<div class="col-md-6 col-sm-6 col-xs-12">
    <select id="grado_id" class="form-control" required onchange="verSeccion()">
        <option value="">Seleccione..</option>
        @foreach ($grado as $g)
            <option value="{{ $g->id }}">{{ $g->nombre }}</option>
        @endforeach
      </select>
</div>