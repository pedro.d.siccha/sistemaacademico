<label class="control-label col-md-3 col-sm-3 col-xs-12">Sección <span class="required">*</span></label>
<div class="col-md-6 col-sm-6 col-xs-12">
  <select id="heard" class="form-control" required>
    <option value="">Seleccione..</option>
    @foreach ($seccion as $s)
        <option value="{{ $s->id }}">{{ $s->nombre }}</option>
    @endforeach
  </select>
</div>