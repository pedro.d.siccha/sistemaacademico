<br />
<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombres <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" readonly value="{{ $estudiante[0]->nombre }}">
        <input type="text" id="estudiante_id" required="required" class="form-control col-md-7 col-xs-12 hidden" readonly value="{{ $estudiante[0]->id }}">
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Apellidos <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="last-name" name="last-name" required="required" class="form-control col-md-7 col-xs-12" readonly value="{{ $estudiante[0]->apellido }}">
    </div>
    </div>
    <div class="form-group">
    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Ultimo Grado Aprobado</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php
            if($estudiante[0]->estado == "NUEVO"){

            }else if($estudiante[0]->estado == "REGISTRADO"){

            }else if($estudiante[0]->estado == "MATRICULADO"){

            }else{
                
            }

            $gr = "";

            if( empty($grado[0]->grado) ){
                echo "<script> swal('ERROR', 'Por favor registre el traslado del alumno', 'info'); </script>";
            }else{
                $gr = $grado[0]->grado;
            }

        ?>
        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name" readonly value="{{ $gr }}">    
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel Académico <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="nivel" class="form-control" required onchange="mostrarGrado()">
            <option value="">Seleccione..</option>
            @foreach ($nivel as $n)
            <option value="{{ $n->id }}">{{ $n->nombre }}</option>
            @endforeach
        </select>
    </div>
    </div>
    <div class="form-group" id="cbGrado">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado Académico <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12" id="cbGrado">
        <select id="grado_id" class="form-control" required>
        <option value="">Seleccione Grado..</option>
        </select>
    </div> 
    </div>
    <div class="form-group" id="cbSeccion">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sección <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12" >
        <select id="seccion_id" class="form-control" required>
        <option value="">Seleccione Seccion..</option>
        </select>
    </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button class="btn btn-primary" type="button" onclick="generarMatricular()">Matricular</button>
        <button class="btn btn-primary" type="reset">Limpiar</button>
        <button type="submit" class="btn btn-success">Cancelar</button>
    </div>
    </div>
</form>

<script>

    function generarMatricular(){ 
        var grado_id = $("#grado_id").val();
        var estudiante_id = $("#estudiante_id").val();
      
        $.post( "{{ Route('matriculaIndividual') }}", {grado_id: grado_id, estudiante_id: estudiante_id, _token:'{{csrf_token()}}'}).done(function(data) {
                      if(data.res != 0){
                        swal("CORRECTO", "El alumno fué natriculado correctamnte", "success");
                        $("#verifAlumno").empty();
                        $("#verifAlumno").html(data.view);
                      }else{
                        swal("ERROR", "Código de error: " + data.resText, "error");
                      }
      
                  });
      }

      function mostrarGrado(){
          var nivel_id = $("#nivel").val();

          $.post( "{{ Route('mostrarGrado') }}", {nivel_id: nivel_id, _token:'{{csrf_token()}}'}).done(function(data) {
                      
            $("#cbGrado").empty();
            $("#cbGrado").html(data.view);

        });
          
      }

        function verSeccion()
        {
            var grado_id = $('#grado_id').val();

            $.post( "{{ Route('verSeccionS') }}", {grado_id: grado_id, _token:'{{csrf_token()}}'}).done(function(data) {
                
                $("#cbSeccion").empty();
                $("#cbSeccion").html(data.view);
                
                

            });

        }

</script>
