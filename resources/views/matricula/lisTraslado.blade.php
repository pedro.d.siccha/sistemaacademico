<table class="table table-hover">
    <thead>
        <tr>
          <th>DNI</th>
          <th>Estudiante</th>
          <th>Cod. Mod. Origen</th>
          <th>I.E. Origen</th>
          <th>Nivel Origen</th>
          <th>Grado Destino</th>
          <th>Año Destino</th>
          <th>Estado</th>
          <th>Resolución</th>
          <th>Administracion</th>
        </tr>
    </thead>
    <tbody>
      <tr>
        @foreach ($traslado as $t)
          <td>{{ $t->dni }}</td>
          <td>{{ $t->nombre }} {{ $t->apellido }} </td>
          <td>{{ $t->codModular }}</td>
          <td>{{ $t->nomColegio }}</td>
          <td>{{ $t->nivel }}</td>
          <td>{{ $t->grado }}</td>
          <td>{{ $t->anio }}</td>
          <td>{{ $t->estado }}</td>
          <td>{{ $t->resolucion }}</td>
          <td><button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="RETIRAR"><i class="fa fa-archive"></i></button></td>    
        @endforeach
    </tr>
    </tbody>
</table>