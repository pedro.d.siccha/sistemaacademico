<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Cursos</th>
            <th>Docente</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($cursos as $c)
            <tr>
                <th scope="row">#</th>
                <td>{{ $c->curso }}</td>
                <td>{{ $c->docente }}</td>
            </tr>    
        @endforeach
    </tbody>
</table>