<br />
<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombres <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Apellidos <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="last-name" name="last-name" required="required" class="form-control col-md-7 col-xs-12">
    </div>
    </div>
    <div class="form-group">
    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Genero</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
    </div>
    </div>
    <div class="form-group">
    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Apoderado</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
    </div>
    </div>
    <div class="form-group">
    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">DNI del Apoderado</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
    </div>
    </div>
    <div class="form-group">
    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Ultimo Grado Aprobado</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel Académico <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="heard" class="form-control" required>
        <option value="">Seleccione..</option>
        <option value="press">Inicial</option>
        <option value="net">Primaria</option>
        <option value="mouth">Secundaria</option>
        </select>
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado Académico <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="heard" class="form-control" required>
        <option value="">Seleccione..</option>
        <option value="press">1ro</option>
        <option value="net">2do</option>
        <option value="mouth">3ro</option>
        <option value="mouth">4to</option>
        <option value="mouth">5to</option>
        <option value="mouth">6to</option>
        </select>
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sección <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="heard" class="form-control" required>
        <option value="">Seleccione..</option>
        <option value="press">A</option>
        <option value="net">B</option>
        <option value="mouth">C</option>
        </select>
    </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button class="btn btn-primary" type="button">Matricular</button>
        <button class="btn btn-primary" type="reset">Limpiar</button>
        <button type="submit" class="btn btn-success">Cancelar</button>
    </div>
    </div>

</form>