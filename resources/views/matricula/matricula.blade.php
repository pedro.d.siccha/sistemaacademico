@extends('layouts.app')
@section('nombrePagina')
 Lista de Pacientes Registrados   
@endsection
@section('contenido')
<div class="clearfix"></div>

<div class="col-md-12 col-sm-6 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Matrícula <small>INDIVIDUAL</small> </h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
      <div class="form-group">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Buscar por DNI, Apellidos y Nombres" id="buscarNombre">
            <span class="input-group-btn">
                <button type="button" class="btn btn-primary" onclick="buscarAlumno()">Buscar!</button>
            </span>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Datos del Alumno <small>Verifique para matricular</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="verifAlumno">
        
      </div>
    </div>
  </div>
</div>

 
@endsection
@section('script')
<script>

function buscarAlumno() {
  var dato = $("#buscarNombre").val();
  
  $.post( "{{ Route('busquedaAlumno') }}", {dato: dato, _token:'{{csrf_token()}}'}).done(function(data) {
              if(data.estado == "NUEVO"){
                swal("ALUMNO NUEVO!", "Por favor registre los datos de traslado", "warning");
              }else if(data.estado == "MATRICULADO"){
                swal("ALUMNO MATRICULADO!", "El alumno ya se encuentra matriculado", "success");
              }else if(data.estado == "REGISTRADO"){
                $("#verifAlumno").empty();
                $("#verifAlumno").html(data.view);
              }else{
                swal("ERROR", "Hubo un error o el alumno no existe", "error");
              }
              
          });
}

function buscarAlumnoMatricula() {
  var alumno = $('#buscarNombre').val();
  
  $.post( "{{ Route('buscarAlumnoMatricula') }}", {alumno: alumno, _token:'{{csrf_token()}}'}).done(function(data) {
                $("#verifAlumno").empty();
                $("#verifAlumno").html(data.view);
            });

}

function mostrarCursos() {
  var grado_id = $("#grado_id").val();

  $.post( "{{ Route('mostrarCursos') }}", {grado_id: grado_id, _token:'{{csrf_token()}}'}).done(function(data) {
                $("#listCursos").empty();
                $("#listCursos").html(data.view);

            });
  
}

function matricular(){ 
  var grado_id = $("#grado_id").val();
  var estudiante_id = $("#idEstudiante").val();
  var periodo_id = $("#periodo_id").val();

  $.post( "{{ Route('matriculaIndividual') }}", {grado_id: grado_id, estudiante_id: estudiante_id, periodo_id: periodo_id, _token:'{{csrf_token()}}'}).done(function(data) {
                $("#listCursos").empty();
                $("#listCursos").html(data.view);

            });
  
}

 

function listaGrado(){

  var nivel = $("#nivelAcademico").val();

$.post( "{{ Route('verNivelAcademico') }}", {nivel: nivel, _token:'{{csrf_token()}}'}).done(function(data) {
                $("#cbGrado").empty();
                $("#cbGrado").html(data.view);

            });

}

</script>
@endsection