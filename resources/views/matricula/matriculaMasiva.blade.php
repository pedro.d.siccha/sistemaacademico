@extends('layouts.app')
@section('nombrePagina')
 Año Escolar   
@endsection
@section('contenido')
<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
<div class="x_title">
    <h2>Matricula <small>MASIVA</small></h2>
    <ul class="nav navbar-right panel_toolbox">
    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <ul class="dropdown-menu" role="menu">
        <li><a href="#">Settings 1</a>
        </li>
        <li><a href="#">Settings 2</a>
        </li>
        </ul>
    </li>
    <li><a class="close-link"><i class="fa fa-close"></i></a>
    </li>
    </ul>
    <div class="clearfix"></div>
</div>
<div class="x_content" id="tabEstudiantes">
        <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
          <thead>
            <tr>
              <th>
                 <th><input type="checkbox" id="check-all" class="flat"></th>
              </th>
              <th>DNI</th>
              <th>Nombres</th>
              <th>Grado y Seccion Anterior</th>
            </tr>
          </thead>


          <tbody>
            @foreach ($estudiante as $e) 
            <tr>
              <td>
                 <th><input type="checkbox" class="flat chekbox_estudiante" name="idEstudiante[]" value="{{ $e->estudiante_id }}" id="idEstudiante"></th>
              </td>
              <td>{{ $e->dni }}</td>
              <td>{{ $e->nombre }} {{ $e->apellido }}</td>
              <td>{{ $e->grado }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
</div>
</div>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Matricula <small>Verifique para matricular</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel Académico <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select id="nivel" class="form-control" required onchange="mostrarGrado()">
                    <option value="">Seleccione..</option>
                    @foreach ($nivel as $n)
                    <option value="{{ $n->id }}">{{ $n->nombre }}</option>
                    @endforeach
                </select>
            </div>
          </div>
          <div class="form-group" id="cbGrado">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado Académico <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12" id="cbGrado">
                <select id="grado_id" class="form-control" required>
                <option value="">Seleccione Grado..</option>
                </select>
            </div>
          </div>
          <div class="form-group" id="cbSeccion">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sección <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12" >
                <select id="seccion_id" class="form-control" required>
                <option value="">Seleccione Seccion..</option>
                </select>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="button" onclick="generarMatriculaMasiva()">Matricular</button>
              <button type="submit" class="btn btn-success">Cancelar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
function generarMatriculaMasiva(){ 

  var grado_id = $("#grado_id").val();
  var idEstudiante = [];

  $('.chekbox_estudiante:checked').each(
  function() {
      idEstudiante.push($(this).val());
    }
  );

  $.post( "{{ Route('generarMatriculaMasiva') }}", { grado_id: grado_id, idEstudiante: idEstudiante, _token:'{{csrf_token()}}'}).done(function(data) {
    if(data.res == 0){
      swal("ERROR ", "Hubo un problema al registrar las secciones \nCodigo de Error: ".data.txtResp, "error");
    }else{
      swal("CORRECTO", "Secciones asignadas correctamente", "success");
    }
    $("#tabEstudiantes").empty();
    $("#tabEstudiantes").html(data.view);

});
}

function mostrarGrado(){
  var nivel_id = $("#nivel").val();

  $.post( "{{ Route('mostrarGrado') }}", {nivel_id: nivel_id, _token:'{{csrf_token()}}'}).done(function(data) {
              
    $("#cbGrado").empty();
    $("#cbGrado").html(data.view);

  });
  
}

function verSeccion()
{
    var grado_id = $('#grado_id').val();

    $.post( "{{ Route('verSeccionS') }}", {grado_id: grado_id, _token:'{{csrf_token()}}'}).done(function(data) {
        
        $("#cbSeccion").empty();
        $("#cbSeccion").html(data.view);
        
        

    });

}


</script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<link href="{{asset('../vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection