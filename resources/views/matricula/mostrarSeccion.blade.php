<select id="seccion_id" class="form-control" required>
    <option value="">Seleccione..</option>
    @foreach ($seccion as $s)
        <option value="{{ $s->seccion_id }}">{{ $s->seccion }}</option>
    @endforeach
</select>