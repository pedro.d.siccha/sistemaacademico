@extends('layouts.app')
@section('nombrePagina')
 Año Escolar   
@endsection
@section('contenido')

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Nominas <small>Generacion y Envio</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="grado_id" class="form-control" required onchange="mostrarSeccion()">
                <option value="">Seleccione..</option>
                @foreach ($grado as $g)
                  <option value="{{ $g->id }}">{{ $g->nombre }}</option>    
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sección <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12" id="seccion">
              <select id="seccion_id" class="form-control" required>
                <option value="">Seleccione..</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Formato <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="heard" class="form-control" required>
                <option value="">Seleccione..</option>
                <option value="press">Nomina de Matricula</option>
                <option value="press">Nomina Adicional</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Responsable de la Matricula <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">R.D. Institucional <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha de Aprobación <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="button" onclick="buscarNomina()"><i class="fa fa-search"></i> BUSCAR</button>
              <button type="submit" class="btn btn-danger"><i class="fa fa-close"></i> CANCELAR</button>
            </div>
          </div>

        </form>
      </div>
      
    </div>
    <div id="tabNomina"></div>
  </div>
</div>


@endsection
@section('script')
<script>
function mostrarSeccion(){ 
   var grado_id = $("#grado_id").val();

  $.post( "{{ Route('mostrarSeccion') }}", {grado_id: grado_id, _token:'{{csrf_token()}}'}).done(function(data) {
                $("#seccion").empty();
                $("#seccion").html(data.view);
            });
}

function buscarNomina(){ 

  $.post( "{{ Route('buscarNomina') }}", { _token:'{{csrf_token()}}'}).done(function(data) {
      $("#tabNomina").empty();
      $("#tabNomina").html(data.view);
  });
  
}

function EliminarPaciente(idPac){ 


}


</script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<link href="{{asset('../vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection