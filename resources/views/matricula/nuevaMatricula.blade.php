<br />
<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombres <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" readonly value="{{ $estudiante[0]->nombre }}">
        <input type="text" id="idEstudiante" hidden class="form-control col-md-7 col-xs-12 hidden" readonly value="{{ $estudiante[0]->id }}">
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Apellidos <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="last-name" name="last-name" required="required" class="form-control col-md-7 col-xs-12" readonly value="{{ $estudiante[0]->apellido }}">
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel Académico <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="periodo_id" class="form-control" required>
        <option value="">Seleccione..</option>
        @foreach ($periodo as $p)
            <option value="{{ $p->id }}">{{ $p->nombre }}</option>    
        @endforeach
        </select>
    </div>
    </div>
    <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado Académico <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select id="grado_id" class="form-control" required onchange="mostrarCursos()">
        <option value="">Seleccione..</option>
        @foreach ($gradoSeccion as $gs)
            <option value="{{ $gs->grado_id }}">{{ $gs->grado }}</option>    
        @endforeach
        </select>
    </div>
    <div id="listCursos">

    </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button class="btn btn-primary" type="button" onclick="matricular()">Matricular</button>
        <button class="btn btn-primary" type="reset">Limpiar</button>
        <button type="submit" class="btn btn-success">Cancelar</button>
    </div>
    </div>
</form>