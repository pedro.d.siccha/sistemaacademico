@extends('layouts.app')
@section('nombrePagina')
 Año Escolar   
@endsection
@section('contenido')

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Traslados <small>Filtros de Busqueda</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo Constancia Vacante <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="heard" class="form-control" required>
                <option value="">Seleccione..</option>
                <option value="net">Por Cambio de Nivel</option>
                <option value="press">Por Cambio de Año</option>
                <option value="net">En el mismo año</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado Constancia <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="heard" class="form-control" required>
                <option value="">Seleccione..</option>
                <option value="press">Registrado</option>
                <option value="net">Constancia recibida de origen</option>
                <option value="mouth">Matriculado en el destino</option>
                <option value="mouth">Constancia anulada</option>
                <option value="mouth">Constancia rechazada</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Año destino <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select id="heard" class="form-control" required>
                <option value="">Seleccione..</option>
                <option value="press">2019</option>
              </select>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="button" onclick="buscarTraslado()">Buscar</button>
              <button type="submit" class="btn btn-success">Limpiar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

<div id="notifGuardar"></div>
<div class="clearfix"></div>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
<div class="x_title">
    <h2><button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="AGREGAR" onclick="agregarTraslado()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="IMPRIMIR"><i class="fa fa-print"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="SALIR"><i class="fa fa-close"></i></button></h2>
    <ul class="nav navbar-right panel_toolbox">
    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <ul class="dropdown-menu" role="menu">
        <li><a href="#">Settings 1</a>
        </li>
        <li><a href="#">Settings 2</a>
        </li>
        </ul>
    </li>
    <li><a class="close-link"><i class="fa fa-close"></i></a>
    </li>
    </ul>
    <div class="clearfix"></div>
</div>
    <div class="x_content" id="tabTraslado">
        <table class="table table-hover">
            <thead>
                <tr>
                  <th>DNI</th>
                  <th>Estudiante</th>
                  <th>Cod. Mod. Origen</th>
                  <th>I.E. Origen</th>
                  <th>Nivel Origen</th>
                  <th>Grado Origen</th>
                  <th>Año Origen</th>
                  <th>Estado</th>
                  <th>Resolución</th>
                  <th>Administracion</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                @foreach ($traslado as $t)
                  <td>{{ $t->dni }}</td>
                  <td>{{ $t->nombre }} {{ $t->apellido }} </td>
                  <td>{{ $t->codModular }}</td>
                  <td>{{ $t->nomColegio }}</td>
                  <td>{{ $t->nivel }}</td>
                  <td>{{ $t->grado }}</td>
                  <td>{{ $t->anio }}</td>
                  <td>{{ $t->estado }}</td>
                  <td>{{ $t->resolucion }}</td>
                  <td><button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="RETIRAR"><i class="fa fa-archive"></i></button></td>    
                @endforeach
            </tr>
            </tbody>
        </table>
    </div>
</div>
</div>

<!-- Modal Agregar Traslado -->
<div class="modal inmodal fade" id="agregarTraslado" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form id="ftraslado" name="ftraslado" method="post" action="guardarTraslado" class="formTraslado" enctype="multipart/form-data">
          <input type="text" name="_token" id="_token" hidden value="{{ csrf_token() }}">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Crear Nueva Area</h4>
          </div>
          <div class="modal-body">
            <div class="form-group  row"><label class="col-sm-2 col-form-label">DNI</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="number" class="form-control" placeholder="Ingrese DNI" id="dni">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" onclick="buscarAlumno()"><i class="fa fa-search"></i></button>
                        <input type="text" class="form-control hidden" id="estudiante_id" name="estudiante_id">
                    </span>
                </div>
              </div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Nombre</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="nombre" readonly></div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Cod. Modular de Origen</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="number" class="form-control" placeholder="Código Modular" id="codModular">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" onclick="buscarColegio()"><i class="fa fa-search"></i></button>
                        <input type="text" class="form-control hidden" id="colegio_id" name="colegio_id">
                    </span>
                </div>
              </div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">IE. Origen</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="nomColegio" readonly name="nomColegio"></div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Año de Origen</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="anio" name="anio"></div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Nivel de Origen</label>
              <div class="col-sm-10">
                <select id="nivel" name="nivel" class="form-control" required onchange="mostrarGrado()">
                  <option>Escoger Nivel...</option>
                  <option value="Inicial">Inicial</option>
                  <option value="Primaria">Primaria</option>
                  <option value="Secundaria">Secundaria</option>
                </select>
              </div>
            </div>
            <div class="form-group row"><label class="col-sm-2 col-form-label">Grado de Origen</label>
              <div class="col-sm-10" id="grado_cb">
                <select id="grado" name="grado" class="form-control" required>
                  <option>Escoger Grado...</option>
                </select>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Numero de Resolución</label>
              <div class="col-sm-10"><input type="file" class="form-control" id="numResolucion" name="numResolucion"></div>
            </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-primary">Registrar Traslado</button>
          </div>
        </form>
      </div>
  </div>
</div>
<!-- Fin Modal Agregar Traslado -->

<!-- Modal Agregar Codigo Modular -->
<div class="modal inmodal fade" id="agregarColegio" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Agregar Colegio</h4>
          </div>
          <div class="modal-body">
            <div class="form-group  row"><label class="col-sm-2 col-form-label">Cod. Modular</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="nCodigoModular" readonly></div>
            </div>
            <div class="form-group  row"><label class="col-sm-2 col-form-label">I. Educativa</label>
              <div class="col-sm-10"><input type="text" class="form-control" id="nColegio"></div>
            </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="guardarColegio()">Guardar</button>
          </div>
      </div>
  </div>
</div>
<!-- Fin Modal Agregar Codigo Modular -->

@endsection
@section('script')
<script>
function agregarTraslado(){ 
  $('#agregarTraslado').modal('show');
}

function mostrarGrado(){
  var nivel = $("#nivel").val();
  
  $.post( "{{ Route('mostrarGradoTraslado') }}", {nivel: nivel, _token:'{{csrf_token()}}'}).done(function(data) {
    $("#grado_cb").empty();
    $("#grado_cb").html(data.view);

  });
  
}

function buscarAlumno(){
  var dni = $("#dni").val();

  $.post( "{{ Route('buscarAlumnoTraslado') }}", {dni: dni, _token:'{{csrf_token()}}'}).done(function(data) {
    if(data.id == "NO"){
      swal("ERROR", "EL Alumno ya se encuantra matriculado o el DNI es incorrecto", "error");
    }else{
      $("#nombre").val(data.nombre + " " + data.apellido);
      $("#estudiante_id").val(data.id);
    }

  });

}

function buscarColegio(){
  var codModular = $("#codModular").val();

  $.post( "{{ Route('buscarColegioTraslado') }}", {codModular: codModular, _token:'{{csrf_token()}}'}).done(function(data) {
    if(data.id == "NO"){
      $("#nCodigoModular").val(data.codModular);
      $('#agregarColegio').modal('show');
    }else{
      $("#nomColegio").val(data.colegio);
      $("#colegio_id").val(data.id);
    }

  });

}

function guardarColegio(){
  var codModular = $("#nCodigoModular").val();
  var colegio = $("#nColegio").val();

  $.post( "{{ Route('guardarColegio') }}", {codModular: codModular, colegio: colegio, _token:'{{csrf_token()}}'}).done(function(data) {
    if(data.id == NO){
      swal("ERROR", "Error al guardar el colegio", "error");
    }else{
      $("#codModular").val(data.codModular);
      $("#nomColegio").val(data.colegio);
      $("#colegio_id").val(data.id);
    }

  });

}

function guardarArea(){ 

  $.post( "{{ Route('guardarTraslado') }}", { _token:'{{csrf_token()}}'}).done(function(data) {
    $('#agregarTraslado').modal('hide');
      $("#notifGuardar").empty();
      $("#notifGuardar").html(data.view);

      $("#tabTraslado").empty();
      $("#tabTraslado").html(data.view2);

  });
}

$(document).on("submit",".formTraslado",function(e){
        
  e.preventDefault();
  var formu = $(this);
  var nombreform = $(this).attr("id");
  
  if (nombreform == "ftraslado") {
      var miurl = "{{ Route('guardarTraslado') }}";
  }
  var formData = new FormData($("#"+nombreform+"")[0]);
  
  $.ajax({ url: miurl, type: 'POST', data: formData, cache: false, contentType: false, processData: false,
          beforeSend: function(){
              setTimeout(function() {
                  toastr.options = {
                      closeButton: true,
                      progressBar: true,
                      showMethod: 'slideDown',
                      timeOut: 4000,
                      positionClass: 'toast-top-center'
                  };
                  toastr.info('Subiendo Archivos, por favor espere');

              }, 1300);
          },
          success: function(data){
            swal("CORRECTO", "El traslado fue registrado correctamente", "success");
            $('#agregarTraslado').modal('hide');
          },
          error: function(data) {
            swal("Error", "Hubo un problema al subir la resolcución", "error");
          }
  });
});

//Guardar Traslado FIN

function buscarTraslado(){ 
  $.post( "{{ Route('buscarTraslado') }}", { _token:'{{csrf_token()}}'}).done(function(data) {
      $("#notifGuardar").empty();
      $("#notifGuardar").html(data.view);
  });

}


</script>
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('../vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<link href="{{asset('../vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
@endsection