@extends('layouts.app')
@section('nombrePagina')
 Contactos   
@endsection
@section('contenido')

<div class="container body">
    <div class="main_container">

      <!-- page content -->
      <div class="" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Contactos</h3>
            </div>

            <div class="title_right">
              <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Buscar contacto...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Buscar!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="clearfix"></div>

          <div class="col-md-4">
            <div class="x_panel">
              <div class="x_title">
                <h2>Contactos <small>conectados</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <ul class="list-unstyled msg_list">
                  <li>
                    <a>
                      <span class="image">
                        <img src="{{ asset('production/images/img.jpg') }}" alt="img" />
                      </span>
                      <span>
                        <span>Raul Gonzales</span>
                        <span class="time">hace 3 minutos</span>
                      </span>
                      <span class="message">
                        Mensaje 001
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Mensaje 001</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <div class="bs-example" data-example-id="simple-jumbotron">
                  <div class="jumbotron">
                    <h1>Buenas tardas!</h1>
                    <p>Este es el primer mensaje.</p>
                  </div>
                </div>

              </div>
            </div>
          </div>

          
        </div>
        <div class="clearfix"></div>
      </div>
      <!-- /page content -->
    </div>
  </div>
  
@endsection