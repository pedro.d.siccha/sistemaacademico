<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//Gestion de Matricula
Route::get('/matricula', 'MatriculaController@matricula')->name('matricula');
Route::post('/buscarAlumnoMatricula', 'MatriculaController@buscarAlumnoMatricula')->name('buscarAlumnoMatricula');
Route::get('/matriculaMasiva', 'MatriculaController@matriculaMasiva')->name('matriculaMasiva');
Route::get('/traslados', 'MatriculaController@traslados')->name('traslados');
Route::get('/nominas', 'MatriculaController@nominas')->name('nominas');
Route::get('/reportes', 'MatriculaController@reportes')->name('reportes');
Route::post('/mostrarCursos', 'MatriculaController@mostrarCursos')->name('mostrarCursos');
Route::post('/matriculaIndividual', 'MatriculaController@matriculaIndividual')->name('matriculaIndividual');
Route::post('/mostrarSeccion', 'MatriculaController@mostrarSeccion')->name('mostrarSeccion');
Route::post('/guardarTraslado', 'MatriculaController@guardarTraslado')->name('guardarTraslado');
Route::post('/buscarTraslado', 'MatriculaController@buscarTraslado')->name('buscarTraslado');
Route::post('/buscarNomina', 'MatriculaController@buscarNomina')->name('buscarNomina');
Route::post('/verNivelAcademico', 'MatriculaController@verNivelAcademico')->name('verNivelAcademico');
Route::post('/verSeccionS', 'MatriculaController@verSeccion')->name('verSeccionS');
Route::post('/busquedaAlumno', 'MatriculaController@busquedaAlumno')->name('busquedaAlumno');
Route::post('/buscarAlumnoTraslado', 'MatriculaController@buscarAlumnoTraslado')->name('buscarAlumnoTraslado');
Route::post('/buscarColegioTraslado', 'MatriculaController@buscarColegioTraslado')->name('buscarColegioTraslado');
Route::post('guardarColegio', 'MatriculaController@guardarColegio')->name('guardarColegio');
Route::post('/mostrarGradoTraslado', 'MatriculaController@mostrarGradoTraslado')->name('mostrarGradoTraslado');
Route::post('/mostrarGrado', 'MatriculaController@mostrarGrado')->name('mostrarGrado');
Route::post('/generarMatriculaMasiva', 'MatriculaController@generarMatriculaMasiva')->name('generarMatriculaMasiva');


//Gestion Academica
Route::get('/anioescolar', 'AcademicoController@anioEscolar')->name('anioEscolar');
Route::post('/crearAnioEscolar', 'AcademicoController@crearAnioEscolar')->name('crearAnioEscolar');
Route::get('/area', 'AcademicoController@area')->name('area');
Route::post('/crearArea', 'AcademicoController@crearArea')->name('crearArea');
Route::get('/planestudios', 'AcademicoController@planEstudios')->name('planEstudios');
Route::get('/grado', 'AcademicoController@grado')->name('grado');
Route::post('/crearGrado', 'AcademicoController@crearGrado')->name('crearGrado');
Route::get('/seccion', 'AcademicoController@seccion')->name('seccion');
Route::post('/crearSeccion', 'AcademicoController@crearSeccion')->name('crearSeccion');
Route::get('/asignacion', 'AcademicoController@asignacion')->name('asignacion');
Route::get('/horarios', 'AcademicoController@horarios')->name('horarios');
Route::post('/asignarCurso', 'AcademicoController@asignarCurso')->name('asignarCurso');
Route::post('/generarHorario', 'AcademicoController@generarHorario')->name('generarHorario');
Route::post('/verHorario', 'AcademicoController@verHorario')->name('verHorario');
Route::get('/nivel', 'AcademicoController@nivel')->name('nivel');
Route::post('/guardarNivel', 'AcademicoController@guardarNivel')->name('guardarNivel');
Route::post('/asignarGrado', 'AcademicoController@asignarGrado')->name('asignarGrado');
Route::post('/verGrados', 'AcademicoController@verGrados')->name('verGrados');
Route::post('/asignarSeccion', 'AcademicoController@asignarSeccion')->name('asignarSeccion');
Route::post('/verSeccion', 'AcademicoController@verSeccion')->name('verSeccion');
Route::post('/mostrarGradoPlanEstudio', 'AcademicoController@mostrarGradoPlanEstudio')->name('mostrarGradoPlanEstudio');
Route::post('/asignarCursos', 'AcademicoController@asignarCursos')->name('asignarCursos');
Route::post('/verGradosDocente', 'AcademicoController@verGradosDocente')->name('verGradosDocente');
Route::post('/verCursosDocente', 'AcademicoController@verCursosDocente')->name('verCursosDocente');
Route::post('/verDocenteDNI', 'AcademicoController@verDocenteDNI')->name('verDocenteDNI');
Route::post('/asignarDocente', 'AcademicoController@asignarDocente')->name('asignarDocente');
Route::post('/mostrarHorario', 'AcademicoController@mostrarHorario')->name('mostrarHorario');

//Gestion de Alumnos
Route::get('/crearEstudiante', 'AlumnoController@crearEstudiante')->name('crearEstudiante');
Route::get('/cambioSeccion', 'AlumnoController@cambioSeccion')->name('cambioSeccion');
Route::get('/retiroEstudiante', 'AlumnoController@retiroEstudiante')->name('retiroEstudiante');
Route::post('/guardarEstudiante', 'AlumnoController@guardarEstudiante')->name('guardarEstudiante');
Route::post('/retirarAlumno', 'AlumnoController@retirarAlumno')->name('retirarAlumno');
Route::post('/mostrarAlumnos', 'AlumnoController@mostrarAlumnos')->name('mostrarAlumnos');
Route::post('/verificarDNI', 'AlumnoController@verificarDNI')->name('verificarDNI');
Route::get('/listaAlumnos', 'AlumnoController@listaAlumnos')->name('listaAlumnos');
Route::post('/verCbGrado', 'AlumnoController@verCbGrado')->name('verCbGrado');
Route::post('/verCbSeccion', 'AlumnoController@verCbSeccion')->name('verCbSeccion');
Route::post('/banAlumno', 'AlumnoController@banAlumno')->name('banAlumno');

//Gestion de Asistencia
Route::get('/asistencia', 'AsistenciaController@asistencia')->name('asistencia');
Route::get('/asistenciadocente', 'AsistenciaController@asistenciaDocente')->name('asistenciaDocente');
Route::post('buscarAlumnoAsistencia', 'AsistenciaController@buscarAlumnoAsistencia')->name('buscarAlumnoAsistencia');
Route::post('enviarAsistencia', 'AsistenciaController@enviarAsistencia')->name('enviarAsistencia');
Route::post('buscarDocenteAsistencia', 'AsistenciaController@buscarDocenteAsistencia')->name('buscarDocenteAsistencia');
Route::post('/verDocenteAsistencia', 'AsistenciaController@verDocenteAsistencia')->name('verDocenteAsistencia');

//Administracion de Cursos
Route::get('/administrarCursos', 'CursoController@administrarCursos')->name('administrarCursos');
Route::post('/panelCursos', 'CursoController@panelCursos')->name('panelCursos');
Route::get('/administrarExamen', 'CursoController@administrarExamen')->name('administrarExamen');
Route::post('/panelExamen', 'CursoController@panelExamen')->name('panelExamen');
Route::get('/administraNotas', 'CursoController@administraNotas')->name('administraNotas');
Route::post('/panelNotas', 'CursoController@panelNotas')->name('panelNotas');
Route::get('/administrarAsistencia', 'CursoController@administrarAsistencia')->name('administrarAsistencia');
Route::post('/panelAsistencia', 'CursoController@panelAsistencia')->name('panelAsistencia');

//Gestion de Calificaciones
Route::get('/calificacion', 'CalificacionesController@calificacion')->name('calificacion');
Route::get('/porcentajecalificacion', 'CalificacionesController@porcentajeCalificacion')->name('porcentajeCalificacion');
Route::get('/acta', 'CalificacionesController@acta')->name('acta');

//Gestion de Finanzas
Route::get('/tipotarifa', 'FinanzasController@tipoTarifa')->name('tipoTarifa');
Route::get('/factura', 'FinanzasController@factura')->name('factura');
Route::get('/pagos', 'FinanzasController@pagos')->name('pagos');
Route::get('/gastos', 'FinanzasController@gastos')->name('gastos');

//Gestion de Notificaciones
Route::get('/noticias', 'NotificacionesController@noticias')->name('noticias');
Route::get('/eventos', 'NotificacionesController@eventos')->name('eventos');
Route::get('/vacaciones', 'NotificacionesController@vacaciones')->name('vacaciones');

//Gestion de Reportes
Route::get('/reporteclases', 'ReportesController@reporteClases')->name('reporteClases');
Route::get('/reporteasistencia', 'ReportesController@reporteAsistencia')->name('reporteAsistencia');
Route::get('/reporteestudiante', 'ReportesController@reporteEstudiante')->name('reporteEstudiante');

//Gestion de Administracion
Route::get('/plantillacorreo', 'AdministradorController@plantillaCorreo')->name('plantillaCorreo');
Route::get('/seguridad', 'AdministradorController@seguridad')->name('seguridad');

//Gestion de Ajustes Generales
Route::get('/controlpersonal', 'AjustesController@controlPersonal')->name('controlPersonal');
Route::get('/ajustesBasicos', 'AjustesController@ajustesBasicos')->name('ajustesBasicos');
Route::post('/guardarTAnio', 'AjustesController@guardarTAnio')->name('guardarTAnio');
Route::post('/guardarTPeriodo', 'AjustesController@guardarTPeriodo')->name('guardarTPeriodo');
Route::post('/guardarTEmpleado', 'AjustesController@guardarTEmpleado')->name('guardarTEmpleado');
Route::post('/guardarParentezco', 'AjustesController@guardarParentezco')->name('guardarParentezco');

//Gestion de Usuario
Route::get('/padres', 'GestionusuariosController@padres')->name('padres');
Route::get('/docentes', 'GestionusuariosController@docentes')->name('docentes');
Route::get('/alumnos', 'GestionusuariosController@alumnos')->name('alumnos');
Route::post('/guardarDocente', 'GestionusuariosController@guardarDocente')->name('guardarDocente');

//Gestion de Mensajes
Route::get('/contactos', 'MensajesController@contactos')->name('contactos');
Route::get('/smsEnviados', 'MensajesController@smsEnviados')->name('smsEnviados');
Route::get('/smsRecibidos', 'MensajesController@smsRecibidos')->name('smsRecibidos');

//Gestion de Apafa
Route::get('/reuniones', 'ApafaController@reuniones')->name('reuniones');
Route::get('/caja', 'ApafaController@caja')->name('caja');
Route::get('/inventario', 'ApafaController@inventario')->name('inventario');

//Gestion de Finanzas
Route::get('/flujoCaja', 'FinanzasController@flujoCaja')->name('flujoCaja');
Route::get('/pagos', 'FinanzasController@pagos')->name('pagos');

